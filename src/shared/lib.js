//https://www.hashtags.org/featured/what-characters-can-a-hashtag-include/
//Keeping it simple for now
exports.validateKeyword = (keyword) => {
  return keyword.length < 15 && /^\w+$/.test(keyword)
}

exports.normaliseWord = (word) => {
  return word.toLowerCase()
}
