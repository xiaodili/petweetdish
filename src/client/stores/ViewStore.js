import {observable} from 'mobx'

const ViewStore = () => {
  const keyword = observable('')
  const runningCulture = observable(false)
  const timeRunning = observable(0)
  const highlightedEntity = observable.box()
  const tweets = observable.shallowArray([])

  //manual testing
  //keyword.set('javascript')
  //tweets.replace([{
    //id: '949892201814855700',
    //timestamp_ms: 1515306923713,
    //text: 'RT @osbi_fr: Bon courage cva et amo. Et bonjour à Guillaume D. venu à ce petit déj\' (un fidèle utilisateur de #pentaho) https://t.co/FZ9xd7…',
    //hashtags: ['javascript', 'ruby', 'lisp']
  //}, {
    //id: '949837998832709600',
    //text: 'Make a simple buzzer app that plays songs using Electron and Johnny-Five! Here I show how the app works in Electron… https://t.co/vbUdTSdqiu',
    //hashtags: ['javascript'],
    //timestamp_ms: 1515294000715
  //}, {
    //id: '949844032892497900',
    //text: 'RT @PopsicleJokez: Lmaooo this video has me dead bro the cat https://t.co/zIz5UakFtV',
    //hashtags: [],
    //timestamp_ms: 1515295439347
  //}])

  return {
    highlightedEntity,
    keyword,
    runningCulture,
    tweets,
    timeRunning,
    addTweet: (tweet) => {
      tweets.push(tweet)
    },
    resetView: () => {
      keyword.set('')
      runningCulture.set(false)
      timeRunning.set(0)
      tweets.clear()
      highlightedEntity.set()
    }
  }
}

export default ViewStore
