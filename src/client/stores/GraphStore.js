import {observable} from 'mobx'
import _ from 'lodash'
import {
  maxComponentSize,
  maxTweetsInNode
} from '../lib/utils.js'
import {
  updateInternalGraph,
  getConnectedComponents,
  Connections
} from '../lib/graph.js'

const GraphStore = () => {
  //internal data structures
  var _links = {}
  var _nodes = {}
  var _connections = Connections()

  const largestCellSize = observable(0)
  const largestColonySize = observable(0)
  const numCells = observable(0)
  const numColonies = observable(0)

  //exposed for D3
  const graph = {
    nodes: [],
    links: []
  }

  const updateStats = () => {
    const components = getConnectedComponents(_.keys(_nodes), _connections)
    numCells.set(graph.nodes.length)
    numColonies.set(components.length)
    largestCellSize.set(maxTweetsInNode(graph.nodes))
    largestColonySize.set(maxComponentSize(components))
  }

  const updateGraph = (tweet) => {
    const {
      nodes,
      links
    } = updateInternalGraph(tweet, _nodes, _links, _connections)
    graph.nodes.push(...nodes)
    graph.links.push(...links)
  }

  return {
    graph,
    numCells,
    numColonies,
    largestCellSize,
    largestColonySize,
    processTweet: (tweet) => {
      updateGraph(tweet)
      updateStats()
    },
    resetGraph: () => {
      _links = {}
      _nodes = {}
      _connections = Connections()
      numCells.set(0)
      numColonies.set(0)
      largestCellSize.set(0)
      largestColonySize.set(0)
      graph.nodes = []
      graph.links = []
    }
  }
}

export default GraphStore
