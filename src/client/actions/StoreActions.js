const TimerService = (elapsedSeconds, predicate) => {
  var current = Date.now()
  const tick = () => {
    elapsedSeconds.set(Math.round((Date.now() - current)/1000))
    if (predicate.get()) {
      window.setTimeout(tick, 1000)
    }
  }
  return {
    reset: () => {
      current = Date.now()
      tick()
    }
  }
}
const StoreActions = (tweetService, viewStore, graphStore, maxTweets) => {
  const timerService = TimerService(viewStore.timeRunning, viewStore.runningCulture)

  const stopListening = () => {
    tweetService.stop(() => {
      viewStore.runningCulture.set(false)
    })
  }

  tweetService.onNewTweet((tweet) => {
    graphStore.processTweet(tweet)
    viewStore.addTweet(tweet)
    if (viewStore.tweets.length >= maxTweets) {
      stopListening()
    }
  })

  return {
    startInnoculation: (keyword) => {
      tweetService.listen(keyword, () => {
        viewStore.runningCulture.set(true)
        viewStore.keyword.set(keyword)
        timerService.reset()
      })
    },
    resetCulture: () => {
      graphStore.resetGraph()
      viewStore.resetView()
    },
    saveCulture: () => {
      //console.log("saving culture");
    },
    stopInnoculation: () => {
      stopListening()
    },
    clearFilter: () => {
      viewStore.highlightedEntity.set()
    },
    selectEntity: (entity) => {
      viewStore.highlightedEntity.set(entity)
    }
  }
}

export default StoreActions
