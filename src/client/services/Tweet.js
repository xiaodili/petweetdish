import {
  CONNECTED_TO_SERVER,
  DISCONNECTED_FROM_SERVER,
  NEW_TWEET
} from '../../shared/enums.js'

const TweetService = (socket) => {
  const fns = []
  socket.on(NEW_TWEET, (msg) => {
    for (const fn of fns) {
      fn(msg)
    }
  })
  return {
    onNewTweet: (fn) => {
      fns.push(fn)
    },
    listen: (keyword, callback) => {
      const fireOnce = () => {
        callback()
        socket.off(CONNECTED_TO_SERVER, fireOnce)
      }
      socket.on(CONNECTED_TO_SERVER, fireOnce)
      socket.io.opts.query = {
        keyword
      }
      socket.connect()
    },
    stop: (callback) => {
      const fireOnce = () => {
        callback()
        socket.off(DISCONNECTED_FROM_SERVER, fireOnce)
      }
      socket.on(DISCONNECTED_FROM_SERVER, fireOnce)
      socket.disconnect()
    }
  }
}

export default TweetService
