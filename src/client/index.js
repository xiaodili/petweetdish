import React from 'react'
import ReactDOM from 'react-dom'
import {AppContainer} from 'react-hot-loader'
import config from '../../config/client'

import ViewStore from './stores/ViewStore.js'
import GraphStore from './stores/GraphStore.js'
import TweetService from './services/Tweet.js'
import StoreActions from './actions/StoreActions.js'
//import Mocket from './lib/Mocket.js'
import io from 'socket.io-client'
import App from './ui'
import jss from './lib/jss.js'
import theme from './ui/styling/theme.js'
import _ from 'lodash'

//const socket = Mocket()
const socket = io(config.serverUrl, {
  autoConnect: false
})
const tweetService = TweetService(socket)
const viewStore = ViewStore()
const graphStore = GraphStore()
const storeActions = StoreActions(tweetService, viewStore, graphStore, config.maxTweets)

window.graphStore = graphStore
window.viewStore = viewStore

const renderReact = () => {
  ReactDOM.render((
    <AppContainer>
      <App 
        graphStore={graphStore}
        viewStore={viewStore}
        storeActions={storeActions}
        tweetLimit={config.maxTweets} />
    </AppContainer>
  ), document.getElementById('app'))
}

//eslint-disable-next-line no-unused-vars
const styles = jss({
  '@global': {
    body: {
      color: theme.colourDarkGrey,
      margin: '0px',
      fontFamily: theme.fontDefault,
      backgroundColor: theme.colourBackground
    }
  }
})

/* eslint-disable */
const renderMisc = () => {
  const miscDom = document.getElementById('misc')
  const button1 = document.createElement('button')
  button1.innerHTML = 'PUSH TWEET'
  miscDom.appendChild(button1)
  const pushEvent = (hashtags) => {
    const event = {
      id: _.random(0, 1000).toString(),
      text: 'Test tweet',
      hashtags,
      timestamp_ms: Date.now()
    }
    console.log("emitting event: ", event);
    socket.triggerEvent('NEW_TWEET', event)
  }
  window.pushEvent = pushEvent
  const btnClicked = () => {
    pushEvent(['testEvent'])
  }
  button1.addEventListener('click', btnClicked)
  //storeActions.startInnoculation('javascript')
  //pushEvent(['javascript'])
  //pushEvent(['javascript', 'ruby', 'lisp'])
  //pushEvent(['dataviz'])
}
/* eslint-enable */

renderReact()
//renderMisc()

// Webpack Hot Module Replacement API
/* eslint-disable no-undef, no-console */
if (module.hot) {
  module.hot.accept('./ui', () => {
    console.log("accepting hot module reload request");
    renderReact()
  })
}
/* eslint-enable no-undef, no-console */
