//import {colours} from './constants.js'

const theme = {
  colourPrimary: '#0084b4', //dark twitter blue
  colourSecondary: '#eaaa67', //warm orange
  colourBackground: '#f6f2f2', //pale orange
  //colourDarkGrey: '#3e3e3e', //dark grey
  colourDarkGrey: '#333333', //dark grey
  fontDefault: "'Open Sans', sans-serif",
  topbarHeight: '40px',
  wideTextMargin: '40px'
}

export default theme
