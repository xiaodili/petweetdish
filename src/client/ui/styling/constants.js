const colours = {
  primary: '#aaaaaa',
  secondary: '#aaaaaaa'
}

const dimensions = {
  vizContainerWidth: 480,
  vizContainerHeight: 480
}

export {
  colours,
  dimensions
}
