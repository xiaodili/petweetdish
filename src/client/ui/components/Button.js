import React from 'react'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'

const styles = (theme) => ({
  btnBase: {
    '&:disabled': {
      cursor: 'default',
      opacity: '0.3'
    },
    border: 'none',
    borderRadius: '5px',
    cursor: 'pointer',
    color: theme.colourBackground,
    fontSize: '12px',
    fontWeight: 'bold',
    padding: '5px 8px',
    margin: '2px',
    textAlign: 'center'
  },
  btnPrimary: {
    backgroundColor: theme.colourPrimary
  },
  btnSecondary: {
    backgroundColor: theme.colourSecondary
  }
})

const BTN_PRIMARY = 'primary'
const BTN_SECONDARY = 'secondary'

class Button extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      classes,
      onClick,
      disabled,
      actionText,
      type
    } = this.props 
    const btnType = (!type || type === BTN_PRIMARY)
      ? classes.btnPrimary
      : classes.btnSecondary

    return (
      <button
        disabled={disabled}
        className={`${classes.btnBase} ${btnType}`}
        onClick={onClick}>
        {actionText}
      </button>
    )
  }
}

Button.propTypes = {
  type: PropTypes.oneOf([BTN_PRIMARY, BTN_SECONDARY]),
  classes: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  actionText: PropTypes.string.isRequired
}

export default injectSheet(styles)(Button)
