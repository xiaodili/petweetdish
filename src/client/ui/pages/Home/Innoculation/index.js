import React from 'react'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import {observer} from 'mobx-react'
import KeywordInput from './KeywordInput.js'
import StatsDisplay from './StatsDisplay.js'

const styles = (theme) => ({
  coolText: {
    color: theme.colourPrimary
  }
})

class Innoculation extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      largestCellSize,
      largestColonySize,
      numCells,
      numColonies,
      resetCulture,
      saveCulture,
      startInnoculation,
      stopInnoculation,
      viewStore
    } = this.props
    const {
      keyword,
      runningCulture,
      timeRunning,
      tweets
    } = viewStore
  
    const shouldShowStats = viewStore.tweets.length !== 0 || viewStore.runningCulture.get()

    return (<div>
      <KeywordInput
        keyword={keyword}
        resetCulture={resetCulture}
        runningCulture={runningCulture}
        saveCulture={saveCulture}
        startInnoculation={startInnoculation}
        stopInnoculation={stopInnoculation}
        tweets={tweets} />
      {shouldShowStats &&
        <StatsDisplay 
          largestCellSize={largestCellSize}
          largestColonySize={largestColonySize}
          numCells={numCells}
          numColonies={numColonies}
          timeRunning={timeRunning} />}
      </div>)
  }
}

Innoculation.propTypes = {
  largestCellSize: PropTypes.object.isRequired,
  largestColonySize: PropTypes.object.isRequired,
  numCells: PropTypes.object.isRequired,
  numColonies: PropTypes.object.isRequired,
  resetCulture: PropTypes.func.isRequired,
  saveCulture: PropTypes.func.isRequired,
  startInnoculation: PropTypes.func.isRequired,
  stopInnoculation: PropTypes.func.isRequired,
  viewStore: PropTypes.object.isRequired
}

export default injectSheet(styles)(observer(Innoculation))
