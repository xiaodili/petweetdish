import React from 'react'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import {observer} from 'mobx-react'
import Button from '../../../components/Button.js'

import {
  validateKeyword
} from '../../../../../shared/lib.js'

const styles = () => ({
  notYetImplemented: {
    display: 'none'
  },
  inputContainer: {
    textAlign: 'center'
  },
  inputField: {
    minWidth: '180px',
    fontSize: '14px',
    lineHeight: '18px',
    //border: `2px solid ${theme.colourDarkGrey}`,
    border: '2px solid #aaa',
    borderRadius: '5px',
    paddingLeft: '2px',
    paddingRight: '2px'
  }
})

class KeywordInput extends React.Component {
  constructor(props) {
    super(props)
    const keyword = props.keyword.get()
    this.state = {
      keyword,
      btnDisabled: !validateKeyword(keyword),
      showValidationMessage: false
    }
  }
  keyPressed(event) {
    if (event.key === 'Enter') {
      if (validateKeyword(this.state.keyword)) {
        this.startInnoculation()
      }
    }
  }
  keywordChanged(event) {
    const keyword = event.target.value
    const showValidationMessage = (keyword !== '' && !validateKeyword(keyword))
    this.setState({
      keyword,
      btnDisabled: !validateKeyword(keyword),
      showValidationMessage
    })
  }
  startInnoculation() {
    this.props.startInnoculation(this.state.keyword)
    this.setState({
      keyword: ''
    })
  }
  render() {
    const {
      classes,
      resetCulture,
      runningCulture,
      saveCulture,
      stopInnoculation,
      tweets
    } = this.props
    const {
      keyword,
      btnDisabled,
      showValidationMessage
    } = this.state

    const shouldShowKeywordInput = !runningCulture.get() && tweets.length === 0
    const shouldShowInputtedKeyword = !shouldShowKeywordInput
    const shouldShowStopInnoculation = runningCulture.get()
    const shouldShowResetInnoculation  = !runningCulture.get() && tweets.length !== 0

    const validationCopy = "Make sure your keyword isn't too long and doesn't contain any spaces or special characters!"


    return (<div className={classes.inputContainer}>
      {shouldShowKeywordInput &&
        <div>
          <div>
            <input
              className={classes.inputField}
              type='text'
              placeholder='e.g. javascript, cats, 2018'
              value={keyword}
              onChange={this.keywordChanged.bind(this)}
              onKeyPress={this.keyPressed.bind(this)} />
            <Button
              disabled={btnDisabled}
              onClick={this.startInnoculation.bind(this)}
              actionText='Nucleate' />
          </div>
          {showValidationMessage && 
            <div>{validationCopy}</div>}
          </div>}
      {shouldShowInputtedKeyword &&
        <div>
          <span><b>Innoculating Keyword:</b> {this.props.keyword.get()}</span>
          {shouldShowStopInnoculation &&
            <Button
              onClick={stopInnoculation}
              type='secondary'
              actionText='Stop' /> }
          {shouldShowResetInnoculation &&
            <span>
              <Button
                onClick={resetCulture}
                type='secondary'
                actionText='Reset' />
              <span
                className={classes.notYetImplemented}>
                <Button
                  onClick={saveCulture}
                  actionText='Save Culture' />
              </span>
            </span>}
        </div>}
    </div>)
  }
}

KeywordInput.propTypes = {
  classes: PropTypes.object.isRequired,
  keyword: PropTypes.object.isRequired,
  resetCulture: PropTypes.func.isRequired,
  runningCulture: PropTypes.object.isRequired,
  saveCulture: PropTypes.func.isRequired,
  startInnoculation: PropTypes.func.isRequired,
  stopInnoculation: PropTypes.func.isRequired,
  tweets: PropTypes.object.isRequired
}

export default injectSheet(styles)(observer(KeywordInput))
