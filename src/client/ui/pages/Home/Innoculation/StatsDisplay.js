import React from 'react'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import {observer} from 'mobx-react'
import moment from 'moment'

const styles = (theme) => ({
  coolText: {
    color: theme.colourPrimary
  }
})

class StatsDisplay extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      largestCellSize,
      largestColonySize,
      numCells,
      numColonies,
      timeRunning
    } = this.props

    const timeRunningSeconds = timeRunning.get()

    return (<div>
      <div><b>Number of Colonies</b>: {numColonies.get()}</div>
      <div><b>Number of Cells</b>: {numCells.get()}</div>
      <div><b>Largest Cell Size</b>: {largestCellSize.get()}</div>
      <div><b>Largest Colony Size</b>: {largestColonySize.get()}</div>
      <div><b>Experiment Started</b>: {moment().subtract(timeRunningSeconds, 's').format('DD/MM/YYYY HH:mm')}</div>
      <div><b>Time Elapsed</b>: {timeRunningSeconds}s</div>
    </div>)
  }
}
StatsDisplay.propTypes = {
  largestCellSize: PropTypes.object.isRequired,
  largestColonySize: PropTypes.object.isRequired,
  numCells: PropTypes.object.isRequired,
  numColonies: PropTypes.object.isRequired,
  timeRunning: PropTypes.object.isRequired
}


export default injectSheet(styles)(observer(StatsDisplay))
