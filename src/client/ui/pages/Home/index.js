import React from 'react'
import PropTypes from 'prop-types'
import injectSheet from 'react-jss'
import {observer} from 'mobx-react'
import Petridish from './Petridish'
import Tweets from './Tweets'
import Innoculation from './Innoculation'

const styles = (theme) => ({
  pageContainer: {
    margin: '5px'
  },
  startContainer: {
    '@media (min-width: 1024px)': {
      marginLeft: theme.wideTextMargin,
      marginRight: theme.wideTextMargin
    }
  },
  paragraph: {
    marginTop: '5px',
    marginBottom: '5px'
  },
  vizContainer: {
    width: '80%',
    textAlign: 'center',
    margin: 'auto',
    '@media (min-width: 1024px)': {
      width: '100%'
    }
  },
  petridishContainer: {
    maxWidth: '480px',
    margin: 'auto',
    '@media (min-width: 1024px)': {
      display: 'inline-block',
      width: '50%',
      verticalAlign: 'top'
      //flexGrow: 1
    }
    //margin: 'auto'
  },
  tweetsContainer: {
    margin: 'auto',
    '@media (min-width: 1024px)': {
      display: 'inline-block',
      width: '50%',
      verticalAlign: 'top'
      //flexGrow: 1
    }
  },
  coolText: {
    color: theme.colourPrimary,
  }
})

class Home extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      classes,
      viewStore,
      graphStore,
      storeActions,
      tweetLimit
    } = this.props
    const {
      resetCulture,
      saveCulture,
      selectEntity,
      startInnoculation,
      stopInnoculation,
      clearFilter
    } = storeActions
    const {
      graph,
      numCells,
      numColonies,
      largestCellSize,
      largestColonySize
    } = graphStore
    const {
      runningCulture,
      tweets,
      highlightedEntity
    } = viewStore

    const shouldShowTweets = runningCulture.get() || tweets.length > 0
    //const vizContainerClassNameString = `${classes.vizContainer} ${shouldShowTweets ? classes.petridishContainerLeft : ''}`

    return (
      <div className={classes.pageContainer}>
        <div className={classes.startContainer}>
          <div className={classes.paragraph}>
            Grow your own Twitter hashtag based culture by supplying a keyword!
          </div>
          <div className={classes.paragraph}>
            Cells will grow in your petridish when people Tweet about your keyword with a hashtag. These hashtag cells develop into colonies when mentioned in the same Tweet.
          </div>
          <div className={classes.paragraph}>
            See what interesting cultures you can develop!
          </div>
          <Innoculation
            largestCellSize={largestCellSize}
            largestColonySize={largestColonySize}
            numCells={numCells}
            numColonies={numColonies}
            resetCulture={resetCulture}
            saveCulture={saveCulture}
            startInnoculation={startInnoculation}
            stopInnoculation={stopInnoculation}
            viewStore={viewStore} />
        </div>
        <div className={classes.vizContainer}>
          <div className={classes.petridishContainer}>
            <Petridish
              selectEntity={selectEntity}
              tweets={tweets}
              graph={graph} />
          </div>
          {shouldShowTweets &&
            <div className={classes.tweetsContainer}>
              <Tweets
                clearFilter={clearFilter}
                highlightedEntity={highlightedEntity}
                tweets={tweets}
                tweetLimit={tweetLimit}
              />
            </div>
          }
        </div>
      </div>)
  }
}
Home.propTypes = {
  classes: PropTypes.object.isRequired,
  graphStore: PropTypes.object.isRequired,
  viewStore: PropTypes.object.isRequired,
  storeActions: PropTypes.object.isRequired,
  tweetLimit: PropTypes.number.isRequired
}

export default injectSheet(styles)(observer(Home))
