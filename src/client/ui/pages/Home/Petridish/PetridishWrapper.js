import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import PetridishD3 from './PetridishD3.js'
import {reaction} from 'mobx'
import injectSheet from 'react-jss'

/* eslint-disable no-unused-vars */
const styles = (theme) => ({
  petridishContainer: {
  }
})
/* eslint-enable no-unused-vars */

//Component Wrapper for the Petridish
//const Petridish = observer(class Petridish extends React.Component {
class PetridishWrapper extends React.Component {
  constructor(props) {
    super(props)
    const {
      graph,
      tweets,
      selectEntity
    } = this.props
    //this.petridishD3 = PetridishD3()
    //Filter this one
    this.petridishD3 = PetridishD3(graph, selectEntity)
    reaction(
      () => tweets.length,
      () => {
        this.petridishD3.update()
      })
  }
  componentDidMount() {
    // eslint-disable-next-line react/no-find-dom-node
    const dom = ReactDOM.findDOMNode(this)
    this.petridishD3.domReady(dom)
    this.petridishD3.update()
  }
  shouldComponentUpdate(nextProps) {
    this.petridishD3.updateLidStatus(nextProps.lidOpen)
    return false
  }
  render() {
    const {
      classes
    } = this.props
    return (
      <div className={classes.petridishContainer}></div>
    )
  }
}

PetridishWrapper.propTypes = {
  classes: PropTypes.object.isRequired,
  graph: PropTypes.object.isRequired,
  lidOpen: PropTypes.bool.isRequired,
  selectEntity: PropTypes.func.isRequired,
  tweets: PropTypes.object.isRequired
}

export default injectSheet(styles)(PetridishWrapper)
