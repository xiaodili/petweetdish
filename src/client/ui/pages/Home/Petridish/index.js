import React from 'react'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'
import PetridishWrapper from './PetridishWrapper.js'
import Button from '../../../components/Button.js'

const styles = () => ({
  petridishContainer: {
    textAlign: 'center'
  }
})

//Component Wrapper for the Petridish
class Petridish extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lidOpen: false
    }
  }
  toggleLid() {
    this.setState({
      lidOpen: !this.state.lidOpen
    })
  }
  render() {
    const {
      graph,
      tweets,
      selectEntity,
      classes
    } = this.props
    const {
      lidOpen
    } = this.state
    return (
      <div className={classes.petridishContainer}>
        <div>
          <PetridishWrapper
            lidOpen={lidOpen}
            graph={graph}
            selectEntity={selectEntity}
            tweets={tweets} />
        </div>
        <Button
          onClick={this.toggleLid.bind(this)}
          type={lidOpen ? 'secondary' : 'primary'}
          actionText={lidOpen ? 'Close Lid' : 'Open Lid'} />
      </div>
    )
  }
}

Petridish.propTypes = {
  classes: PropTypes.object.isRequired,
  graph: PropTypes.object.isRequired,
  selectEntity: PropTypes.func.isRequired,
  tweets: PropTypes.object.isRequired
}

export default injectSheet(styles)(observer(Petridish))
