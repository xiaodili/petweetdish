//D3
import * as d3 from 'd3'

//JSS
import jss from '../../../../lib/jss.js'

import {
  dimensions
} from '../../../styling/constants.js'

const width = dimensions.vizContainerWidth
const height = dimensions.vizContainerHeight
const petridishRadius = (height - 10)/2
const nodeRadius = 5
const nodeStrokeWidth = 2
const dishStrokeWidth = 4
const strokeOverflow = (nodeStrokeWidth + dishStrokeWidth)/2
const d3Styles = jss({
  node: {
    fill: '#bbb',
    stroke: '#555',
    strokeWidth: nodeStrokeWidth,
    cursor: 'pointer'
  },
  lid: {
    fill: '#000',
    opacity: '0.2',
    stroke: '#aaa',
    strokeWidth: dishStrokeWidth
  },
  link: {
    stroke: '#777',
    strokeWidth: 2
  },
  petridish: {
    fill: '#fff',
    stroke: '#aaa',
    strokeWidth: dishStrokeWidth
  }
})

//Maybe these colours are too colourful
//const HASH_LENGTH = Math.pow(2, 20) - 1;

//function getHash(str) {
  //return crc32(str) & HASH_LENGTH;
//}

//const color = d3.scaleRainbow()
//const color = d3.scaleOrdinal(d3.schemeCategory10)
//
//const getColor = (d) => {
  //return color(d.name)
  //return color(getHash(d.name))
//}

const nodeStyle = `.${d3Styles.node}`
const linkStyle = `.${d3Styles.link}`
const lidStyle = `.${d3Styles.lid}`
const getLinkId = (d) => {
  return d.id
}
const getLinkSourceX = (d) => {
  return d.source.x
}
const getId = (d) => {
  return d.id
}
const computeRadius = (d) => {
  return d.tweets.length * nodeRadius
}
const getLinkSourceY = (d) => {
  return d.source.y
}
const getLinkTargetX = (d) => {
  return d.target.x
}
const getLinkTargetY = (d) => {
  return d.target.y
}
const getX = (d) => {
  return d.x
}
const getY = (d) => {
  return d.y
}
const Petridish = (graph, selectEntity) => {
  let nodeElements,
    linkElements,
    simulation,
    svg,
    domReady;
  let lidOpen = false

  const onNodeClicked = (d) => {
    selectEntity(d)
  }

  const petridishBound = () => {
    for (const node of graph.nodes) {
      const r = [node.x - width/2 + node.vx, node.y - height/2 + node.vy]
      const radius = Math.sqrt(r[0] * r[0] + r[1] * r[1])
      const borderOverflow = radius + computeRadius(node) + strokeOverflow - petridishRadius
      if (borderOverflow > 0) {
        //get radius vector and scale it back
        const totalR = Math.abs(r[0]) + Math.abs(r[1])
        const vxAdjust = (borderOverflow/totalR) * Math.abs(r[0])
        const vyAdjust = (borderOverflow/totalR) * Math.abs(r[1])
        if (r[0] > 0) {
          node.vx -= vxAdjust
        } else {
          node.vx += vxAdjust
        }
        if (r[1] > 0) {
          node.vy -= vyAdjust
        } else {
          node.vy += vyAdjust
        }
      }
    }
  }

  function dragStarted(d) {
    if (lidOpen) {
      if (!d3.event.active) {
        simulation.alphaTarget(0.3).restart()
      }
      d.fx = d.x
      d.fy = d.y
    }
  }

  function dragged(d) {
    if (lidOpen) {
      d.fx = d3.event.x
      d.fy = d3.event.y
    }
  }

  function dragEnded(d) {
    if (lidOpen) {
      if (!d3.event.active) {
        simulation.alphaTarget(0)
      }
      d.fx = null
      d.fy = null
    }
  }

  function ticked() {
    nodeElements
      .attr("cx", getX)
      .attr("cy", getY)

    linkElements
      .attr("x1", getLinkSourceX)
      .attr("y1", getLinkSourceY)
      .attr("x2", getLinkTargetX)
      .attr("y2", getLinkTargetY)
  }

  function restart() {

    nodeElements = nodeElements.data(graph.nodes, getId)

    nodeElements.exit().transition()
      .attr("r", 0)
      .remove()

    nodeElements = nodeElements
      .enter()
      .append("circle")
      .attr('class', d3Styles.node)
      .attr('r', 0)
      .merge(nodeElements)
      .on('click', onNodeClicked)
      .call(d3.drag()
        .on("start", dragStarted)
        .on("drag", dragged)
        .on("end", dragEnded))

    nodeElements
      .transition().attr('r', computeRadius)
    
    linkElements = linkElements.data(graph.links, getLinkId)

    linkElements.exit()
      .remove()

    linkElements = linkElements
      .enter()
      .append("line")
      .attr('class', d3Styles.link)
      //.call(function(l) { l.transition().attr("stroke-opacity", 1); })
      .merge(linkElements)

    simulation.nodes(graph.nodes)
    //Accesses the link force and assigns it the links. Needs to be done after the nodes have started ticking
    simulation.force("link").links(graph.links)
    //simulation.restart()
    simulation.alpha(1).restart()
  }

  return {
    domReady: (dom) => {
      svg = d3.select(dom).append('svg')
        .attr('preserveAspectRatio', 'xMinYMin meet')
        .attr('viewBox', `0 0 ${width} ${height}`)

      //Petridish
      svg.append('circle')
        .attr('cx', width/2)
        .attr('cy', height/2)
        .attr('r', petridishRadius)
        .attr('class', d3Styles.petridish)

      //everything else
      const g = svg.append("g");
      linkElements = g.append("g").selectAll(linkStyle);
      nodeElements = g.append("g").selectAll(nodeStyle);

      //Petridish
      svg.append('circle')
        .attr('cx', width/2)
        .attr('cy', height/2)
        .attr('r', petridishRadius)
        .attr('class', d3Styles.lid)

      simulation = d3.forceSimulation()
        .force('charge', d3.forceManyBody().strength(-150))
        .force('link', d3.forceLink().id(getId))
        .force('center', d3.forceCenter(width/2, height/2))
        .force('forceX', d3.forceX(width/2))
        .force('forceY', d3.forceY(width/2))
        .force('nodeCollision', d3.forceCollide().radius(computeRadius))
        //.force('radial', d3.forceRadial(petridishRadius))
        .force('petridishBound', petridishBound)
        //.alphaTarget(1)
        .on('tick', ticked)

      domReady = true
    },
    updateLidStatus: (_lidOpen) => {
      lidOpen = _lidOpen
      if (lidOpen) {
        svg.select(lidStyle).transition().attr('cx', -width/2)
      } else {
        svg.select(lidStyle).transition().attr('cx', width/2)
      }
    },
    update: () => {
      if (domReady) {
        restart()
      }
    }
  }
}

export default Petridish
