import React from 'react'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import moment from 'moment'

const styles = () => ({
  tweetContainer: {
    borderRadius: '5px',
    backgroundColor: 'white',
    padding: '5px',
    marginTop: '5px',
    marginBottom: '5px'
    //boxSizing: 'border-box'
  },
  externalLink: {
    cursor: 'pointer',
    width: '16px'
  },
  tweetHeader: {
    display: 'flex',
    justifyContent: 'space-between'
  }
})

//Component Wrapper for the Petridish
class Tweet extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      classes,
      id,
      text,
      hashtags,
      timestamp_ms
    } = this.props

    const timeFormatted = moment(timestamp_ms).format('DD/MM/YYYY HH:mm')
    const hashTagsFormatted = hashtags.map((hashtag) => {
      return `#${hashtag}`
    }).join(' ')

    //https://twittercommunity.com/t/link-to-a-tweet-based-on-id/70013/2
    return (
      <div className={classes.tweetContainer}>
        <div className={classes.tweetHeader}>
          <span><b>{timeFormatted}</b></span>
          <a href={`http://twitter.com/-/status/${id}`} target='_blank'><img className={classes.externalLink} src='/assets/external_link.svg'/></a>
        </div>
        <div>{text}</div>
        <div><b>{hashTagsFormatted}</b></div>
      </div>
    )
  }
}

Tweet.propTypes = {
  classes: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  hashtags: PropTypes.arrayOf(PropTypes.string).isRequired,
  timestamp_ms: PropTypes.number.isRequired
}

export default injectSheet(styles)(Tweet)
