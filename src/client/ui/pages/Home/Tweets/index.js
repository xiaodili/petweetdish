import React from 'react'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'
import Tweet from './Tweet'
import Filter from './Filter'

const styles = () => ({
  tweetLimit: {
    fontSize: 'smaller',
    marginLeft: '5px'
  },
  tweetsContainer: {
    margin: '5px',
    textAlign: 'left'
  },
  tweetsHeaderContainer: {
  },
  tweetsHeader: {
    fontSize: '30px',
    fontWeight: 'bold'
  }
})

const getTweetsToShow = (tweets, highlightedEntity) => {
  if (!highlightedEntity.get()) {
    return tweets
  } else {
    const hlTweetLookup = new Set(highlightedEntity.get().tweets.peek())
    return tweets.filter((tweet) => {
      return hlTweetLookup.has(tweet.id)
    })
  }
}

//Component Wrapper for the Petridish
class Tweets extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      classes,
      tweets,
      highlightedEntity,
      tweetLimit,
      clearFilter
    } = this.props

    const shouldShowFilter = highlightedEntity.get()
    const tweetsToShow = getTweetsToShow(tweets, highlightedEntity)

    return (
      <div className={classes.tweetsContainer}>
        <div className={classes.tweetsHeaderContainer}>
          <div>
            <span className={classes.tweetsHeader}>Tweets</span>
            <span className={classes.tweetLimit}>{`(${tweets.length}/${tweetLimit} full)`}</span>
          </div>
          {shouldShowFilter && (<Filter
            highlightedEntity={highlightedEntity}
            clearFilter={clearFilter}
            tweets={tweets}
          />)}

        </div>
        {tweetsToShow.reverse().map((tweet) => {
          //mobx's reverse() is pure so the above is fine
          return (<Tweet
            key={tweet.id}
            id={tweet.id}
            text={tweet.text}
            hashtags={tweet.hashtags}
            timestamp_ms={tweet.timestamp_ms} />)
        })}
      </div>
    )
  }
}

Tweets.propTypes = {
  classes: PropTypes.object.isRequired,
  clearFilter: PropTypes.func.isRequired,
  highlightedEntity: PropTypes.object.isRequired,
  tweets: PropTypes.object.isRequired,
  tweetLimit: PropTypes.number.isRequired
}

export default injectSheet(styles)(observer(Tweets))
