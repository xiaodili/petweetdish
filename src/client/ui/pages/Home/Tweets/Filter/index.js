import React from 'react'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'
import {observer} from 'mobx-react'

const styles = () => ({
  filterContainer: {
    alignItems: 'flex-end',
    display: 'flex',
    justifyContent: 'space-between'
  },
  clearFilterLink: {
    fontSize: 'smaller',
    cursor: 'pointer',
    textDecoration: 'underline'
  },
  filterDescription: {
    fontSize: 'smaller'
  }
})

const getFilterDescriptionString = (highlightedEntity) => {
  const unboxed = highlightedEntity.get()
  if (unboxed.source) { //TODO: link  (hook up in d3)
    return `showing Tweets with #${unboxed.source} and #${unboxed.target}`
  } else {
    return `showing Tweets with #${unboxed.id}`
  }
}

const getFilterStatString = (highlightedEntity, tweets) => {
  return `(${highlightedEntity.get().tweets.length}/${tweets.length})`
}

class Filter extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      classes,
      clearFilter,
      highlightedEntity,
      tweets
    } = this.props

    const filterDescString = getFilterDescriptionString(highlightedEntity)
    const filterStatString = getFilterStatString(highlightedEntity, tweets)

    return (
      <div className={classes.filterContainer}>
        <div className={classes.filterDescription}>
          {filterDescString} {filterStatString}
        </div>
        <div className={classes.clearFilterLink} onClick={clearFilter}>
          Clear Filter
        </div>
      </div>)
  }
}

Filter.propTypes = {
  classes: PropTypes.object.isRequired,
  clearFilter: PropTypes.func.isRequired,
  highlightedEntity: PropTypes.object.isRequired,
  tweets: PropTypes.object.isRequired
}

export default injectSheet(styles)(observer(Filter))
