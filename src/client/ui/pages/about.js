import React from 'react'
import injectSheet from 'react-jss'
import PropTypes from 'prop-types'

const styles = (theme) => ({
  quote: {
    fontStyle: 'oblique'
  },
  pageContainer: {
    margin: '5px'
  },
  link: {
    color: theme.colourPrimary,
    '&:visited': {
      color: theme.colourPrimary
    }
  },
  restContainer: {
    '@media (min-width: 1024px)': {
      marginLeft: theme.wideTextMargin,
      marginRight: theme.wideTextMargin
    }
  },
  quoteContents: {
    marginLeft: '25px'
  },
  blockQuote: {
    '&:before': {
      color: theme.colourDarkGrey,
      content: 'open-quote',
      fontSize: '50px',
      lineHeight: '5px',
      marginRight: '0.25em',
      verticalAlign: '-0.4em',
      fontFamily: 'serif'
    },
    background: '#f9f9f9',
    borderLeft: `10px solid ${theme.colourDarkGrey}`,
    marginLeft: '0px',
    marginRight: '0px',
    padding: '5px 10px'
  }
})

const About = ({classes}) => (
  <div className={classes.pageContainer}>
    <div className={classes.restContainer}>
      <blockquote className={classes.blockQuote}>
        <div className={classes.quoteContents}>
          <div className={classes.quote}>How inscrutable and incomprehensible are the hidden works of Nature!</div>
          <div>
            - Antonie van Leeuwenhoek
          </div>
        </div>
      </blockquote>
      <p>
        Petweetdish has been an exercise in making something neat with the Twitter Streaming API; the concept is inspired by the molecular biology projects done at the <a className={classes.link} href='https://biohackspace.org/' rel='noopener noreferrer' target='_blank'> London Biohackspace</a>.
      </p>
      <p>
        The link to the source code will be included here soon.
      </p>
    </div>
  </div>)

About.propTypes = {
  classes: PropTypes.object.isRequired
}

export default injectSheet(styles)(About)
