import React from 'react'
import PropTypes from 'prop-types'
import { NavLink, Link } from 'react-router-dom'
import injectSheet from 'react-jss'

const styles = (theme) => ({
  topbarContainer: {
    backgroundColor: theme.colourSecondary,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: theme.topbarHeight
  },
  linkContainer: {
  },
  logo: {
    height: theme.topbarHeight

  },
  navLinkHome: { //Hide Home link if screen too small
    //display: 'none',
    //'@media (min-width: 480px)': {
      //display: 'inline'
    //}
  },
  navLink: {
    '&:visited': {
      color: theme.colourDarkGrey
    },
    fontSize: 'small',
    color: theme.colourDarkGrey,
    marginLeft: '5px',
    marginRight: '5px',
    textDecoration: 'none',
    '@media (min-width: 480px)': {
      fontSize: 'medium'
    }
  },
  matchedLink: {
    textDecoration: 'underline'
  }
})

class Topbar extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {
      classes
    } = this.props
    return (
      <div className={classes.topbarContainer}>
        <Link to='/' className={classes.logo}>
          <img src='/assets/logo_noembed.svg'/>
        </Link>
        <div className={classes.linkContainer}>
          <NavLink exact className={`${classes.navLink} ${classes.navLinkHome}`} activeClassName={classes.matchedLink} to="/">Home</NavLink>
          <NavLink className={classes.navLink} activeClassName={classes.matchedLink} to="/about">About</NavLink>
        </div>
      </div>
    )
  }
}

Topbar.propTypes = {
  classes: PropTypes.object.isRequired
}

export default injectSheet(styles)(Topbar)
