import React from 'react'
import PropTypes from 'prop-types'
import Home from './pages/Home'
import About from './pages/About.js'
import theme from './styling/theme.js'
import {ThemeProvider} from 'react-jss'
import {Route, BrowserRouter} from 'react-router-dom'
import Topbar from './Topbar.js'

//https://github.com/ReactTraining/react-router/issues/4105
const App = ({graphStore, viewStore, storeActions, tweetLimit}) => (
  <BrowserRouter>
    <ThemeProvider theme={theme}>
      <div>
        <Topbar />
        <div>
          <Route exact path="/" render={(props) => (
              <Home
                graphStore={graphStore}
                viewStore={viewStore}
                storeActions={storeActions}
                tweetLimit={tweetLimit}
                {...props}/>)} />
          <Route path="/about" component={About}/>
        </div>
      </div>
    </ThemeProvider>
  </BrowserRouter>
)

App.propTypes = {
  graphStore: PropTypes.object.isRequired,
  viewStore: PropTypes.object.isRequired,
  storeActions: PropTypes.object.isRequired,
  tweetLimit: PropTypes.number.isRequired
}

export default App
