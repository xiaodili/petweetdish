import {
  dimensions
} from '../ui/styling/constants.js' //for node initialisation
import {
  observable
} from 'mobx'

//Order of magnitude faster than (arr.length !== 0 ? _.maxBy(arr, 'length').length : 0)
const maxComponentSize = (arr) => {
  if (arr.length === 0) {
    return 0
  }
  let m = arr[0].length
  for (let i = 1; i < arr.length; i++) {
    let v = arr[i].length
    if (v > m) {
      m = v
    }
  }
  return m
}

const maxTweetsInNode = (nodes) => {
  if (nodes.length === 0) {
    return 0
  }
  let m = nodes[0].tweets.length
  for (let i = 1; i < nodes.length; i++) {
    let v = nodes[i].tweets.length
    if (v > m) {
      m = v
    }
  }
  return m
}

//[1,2,3] → [[1,2], [2,3], [1,3]]
const getPairCombinations = (xs) => {
  const combinations = []
  for (let i = 0; i < xs.length - 1; i++) {
    for (let j = i + 1; j < xs.length; j++) {
      combinations.push([xs[i], xs[j]])
    }
  }
  return combinations
}

//'apples', 'bananas' → {linkId: 'apples-bananas', source: 'apples', target: 'bananas'}
//'bananas', 'apples' → {linkId: 'apples-bananas', source: 'apples', target: 'bananas'}
const generateLinkData = (a, b) => {
  return a < b //Lexicographically ordered with a hyphen in the middle. Should be alright!
    ? {linkId: `${a}-${b}`, source: a, target: b}
    : {linkId: `${b}-${a}`, source: b, target: a}
}

const createNode = (id) => {
  return {
    id,
    tweets: observable([]),
    x: dimensions.vizContainerWidth/2,
    y: dimensions.vizContainerHeight/2
  }
}

//'a-b', 'a', 'b' → {id: 'a-b', source: 'a', target: 'b', tweets: []}
const createLink = (id, a, b) => {
  return {id, source: a, target: b, tweets: observable([])}
}

export {
  createLink,
  createNode,
  generateLinkData,
  getPairCombinations,
  maxComponentSize,
  maxTweetsInNode
}
