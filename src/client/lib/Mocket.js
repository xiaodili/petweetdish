/* eslint-disable */
const Mocket = () => {
  const fns = {}
  return {
    //API
    on: (eventName, fn) => {
      if (!fns[eventName]) {
        fns[eventName] = new Set()
      } 
      fns[eventName].add(fn)
      console.log(`event ${eventName} registered for`);
    },
    off: (eventName, fn) => {
      console.log(`removing listener for ${eventName}`);
      if (fns[eventName]) {
        fns[eventName].delete(fn)
      }
    },
    connect: () => {
      if (fns.connect) {
        for (const fn of fns['connect']) {
          fn()
        }
      }
    },
    disconnect: () => {
      if (fns.disconnect) {
        for (const fn of fns['disconnect']) {
          fn()
        }
      }
    },
    emit: (eventName, args) => {
      console.log(`event ${eventName} emitted with ${args}`);
    },
    //for pushing
    triggerEvent: (eventName, body) => {
      for (const fn of fns[eventName]) {
        fn(body)
      }
    }
  }
}

export default Mocket
