//Functions for graph processing
import _ from 'lodash'

import {
  getPairCombinations,
  generateLinkData,
  createNode,
  createLink
} from './utils.js'

import {
  normaliseWord
} from '../../shared/lib.js'

// wrapper around maintaining bidirectional edges; does NOT guarantee unique list
const Connections = (edges = {}) => {
  return {
    add: (id1, id2) => {
      if (!edges[id1]) {
        edges[id1] = []
      }
      if (!edges[id2]) {
        edges[id2] = []
      }
      edges[id1].push(id2)
      edges[id2].push(id1)
    },
    get: (id) => {
      return edges[id] || []
    },
    numEdges: () => {
      return _.keys(edges).map((id) => {
        return edges[id].length
      }).reduce(_.add, 0)/2
    }
  }
}

//Updates internal representation of graph, returns any new nodes or links to be inserted
//tweet, nodes!, links!, connections! → {nodes <[node...], links} <[link...]>}
const updateInternalGraph = (tweet, nodes, links, connections) => {
  const newNodes = []
  const newLinks = []
  const {
    id,
    hashtags
  } = tweet

  const hashtagsNormalised = hashtags.map(normaliseWord)

  //nodes
  for (const hashtag of hashtagsNormalised) {
    if (!nodes[hashtag]) {
      const node = createNode(hashtag)
      nodes[hashtag] = node
      newNodes.push(node)
    }
    nodes[hashtag].tweets.push(id)
  }

  //edges
  const hashtagPairs = getPairCombinations(hashtagsNormalised)
  for (const hashtagPair of hashtagPairs) {
    const {
      linkId,
      source,
      target
    } = generateLinkData(hashtagPair[0], hashtagPair[1])
    if (!links[linkId]) {
      const link = createLink(linkId, source, target)
      links[linkId] = link
      newLinks.push(link)
      connections.add(source, target)
    }
    links[linkId].tweets.push(id)
  }

  return {
    nodes: newNodes,
    links: newLinks
  }
}

//{a: 0, b: 1, c: 2}, [a, b, c, d, e, f] → f
const getFirstUnvisitedId = (visitedNodes, nodeIds) => {
  return nodeIds.find((id) => !(id in visitedNodes))
}

//{a: 0, b: 1, c: 2, d: 2} → [[a], [b], [c, d]]
const visitedNodesToComponents = (visitedNodes) => {
  return _.values(_.groupBy(_.keys(visitedNodes), (k) => visitedNodes[k]))
}

//DFS to get connected components
// [a, b, c, d], Connections({a: [b], b: [a]}) →  [[a, b], [c], [d]]
const getConnectedComponents = (nodeIds, connections) => {
  const visitedNodes = {}
  const numNodes = nodeIds.length
  let islandId = 0

  const visit = (nodeId) => {
    visitedNodes[nodeId] = islandId
    for (const targetId of connections.get(nodeId)) {
      if (!(targetId in visitedNodes)) {
        visit(targetId)
      }
    }
  }

  while(_.keys(visitedNodes).length !== numNodes) {
    visit(getFirstUnvisitedId(visitedNodes, nodeIds))
    islandId += 1
  }
  return visitedNodesToComponents(visitedNodes)
}

export {
  updateInternalGraph,
  getConnectedComponents,
  Connections
}
