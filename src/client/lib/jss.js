import jss from 'jss'
import preset from 'jss-preset-default'
jss.setup(preset())

//https://github.com/cssinjs/jss#example
const createJssClasses = (styles) => {
  return jss.createStyleSheet(styles).attach().classes
}

export default createJssClasses
