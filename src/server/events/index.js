const logger = require('../lib/log.js')
const {
  DISCONNECTED_FROM_SERVER,
  CLIENT_CONNECTED,
  NEW_TWEET
} = require('../../shared/enums.js')
const {
  normaliseWord,
  validateKeyword
} = require('../../shared/lib.js')
const {
  getTrackedKeywords
} = require('../lib/sockets.js')

const {
  isTweet,
  extractRelevantTexts,
  processTweet
} = require('../lib/tweets.js')

const _ = require('lodash')

const Twitter = require('../services/twitter.js')

const updateTwitterStream = (io, twitterClient) => {
  const getKeywords = () => {
    return _.uniq(getTrackedKeywords(io))
  }
  twitterClient.track(getKeywords)
}

const getRelevantRoomsToBroadcast = (keywords, tweet) => {
  const texts = extractRelevantTexts(tweet)
  return keywords.filter((keyword) => {
    //https://stackoverflow.com/questions/12536259/regexp-showing-unexpected-type-error-in-javascript
    const regex = new RegExp(keyword, 'i')
    const contains = regex.test.bind(regex)
    return _.some(texts, contains)
  })
}

const handleEvents = (twitterConfig, io, stats) => {

  const onNewTweet = (tweet) => {
    if (isTweet(tweet)) {
      if (tweet.entities.hashtags.length === 0) {
        stats.numHashtaglessTweets += 1
        //logger.log("discarding Tweet since no hashtags have been used")
      } else {
        const keywords = getTrackedKeywords(io)
        const rooms = getRelevantRoomsToBroadcast(keywords, tweet)
        if (rooms.length === 0) {
          //logger.log("discarding Tweet since can't find any mentioned keywords in any text fields");
          //console.log("JSON.stringify(tweet, null, 2): ", JSON.stringify(tweet, null, 2));
          stats.numUnroutedTweets += 1
        } else {
          let recipientSockets = io
          for (const room of rooms) {
            recipientSockets = recipientSockets.to(room)
          }
          recipientSockets.emit(NEW_TWEET, processTweet(tweet))
        }
      }
    }
  }

  const twitterClient = Twitter(twitterConfig, onNewTweet)

  io.on(CLIENT_CONNECTED, (socket) => {
    const keyword = socket.handshake.query.keyword
    logger.log(`client connected with ${keyword}`)
    if (validateKeyword(keyword)) {
      socket.keyword = normaliseWord(keyword)
      socket.join(keyword)
      updateTwitterStream(io, twitterClient)
    }
    socket.on(DISCONNECTED_FROM_SERVER, () => {
      updateTwitterStream(io, twitterClient)
    })
  })
}

module.exports = handleEvents
