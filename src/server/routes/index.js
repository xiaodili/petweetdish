const StatusHandler = require('./status/index.js')

module.exports = (app, io, stats) => {
  const statusHandler = StatusHandler(io, stats)
  app.get('/api/status', statusHandler.get)
}
