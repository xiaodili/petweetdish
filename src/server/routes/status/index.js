const {
  getTrackedKeywords,
  getNumConnections
} = require('../../lib/sockets.js')

const StatusHandler = (io, stats) => {
  return {
    get: (req, res) => {
      res.set('Content-Type', 'application/json')
      res.send(JSON.stringify({
        numConnections: getNumConnections(io),
        trackedKeywords: getTrackedKeywords(io),
        uptime: Math.floor(process.uptime()),
        numHashtaglessTweets: stats.numHashtaglessTweets,
        numUnroutedTweets: stats.numUnroutedTweets
      }))
    }
  }
}

module.exports = StatusHandler
