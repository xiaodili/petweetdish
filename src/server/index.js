const express = require('express')
const path = require('path')
const http = require('http')
const socket = require('socket.io')
const routes = require('./routes/index.js')
const events = require('./events/index.js')
const config = require('../../config/server/index.js')
const logger = require('./lib/log.js')

const Server = () => {
  const app = express()
  const server = http.Server(app)
  return {
    start: (static_dir, assets_dir) => {
      const io = socket(server)
      const stats = {
        numHashtaglessTweets: 0,
        numUnroutedTweets: 0
      }
      routes(app, io, stats)
      events(config.twitterConfig, io, stats)
      app.use('/', express.static(static_dir))
      app.use('/assets', express.static(assets_dir))
      app.use('*', express.static(static_dir))
      logger.log(`server listening on ${config.serverPort}`);
      logger.log(`serving from ${static_dir}`);
      logger.log(`serving from ${assets_dir}`);
      server.listen(config.serverPort)
    }
  }
}

if (require.main === module) {
  const server = Server()
  const static_dir = path.join(__dirname, '../../dist/client')
  const assets_dir = path.join(__dirname, '../../dist/assets')
  server.start(static_dir, assets_dir)
}

module.exports = Server
