const Twitter = require('twitter')
const logger = require('../lib/log.js')

//https://github.com/desmondmorris/node-twitter/blob/master/lib/twitter.js#L277
const ENCHANCE_YOUR_CALM_ERROR_MESSAGE = 'Status Code: 420'

module.exports = (config, callback) => {
  const client = new Twitter(config)
  let stream = null

  const tryTrack = (getKeywords) => {
    const keywords = getKeywords()
    logger.log("keywords: ", keywords);
    if (keywords.length === 0) {
      if (stream) {
        stream.destroy() //Hope this is synchronous!
        stream = null
      }
    } else {
      if (stream) {
        stream.destroy()
      }
      stream = client.stream('/statuses/filter', {
        track: keywords.join(',')
      })
      stream.on('error', (err) => {
        if (err.message === ENCHANCE_YOUR_CALM_ERROR_MESSAGE) {
          logger.log("rate limit hit; waiting 2s before trying again")
          setTimeout(tryTrack, 2000, getKeywords)
        } else {
          logger.log("uh oh, not sure how to deal with this:")
          logger.log("err.message: ", err.message)
        }
      })
      stream.on('data', callback)
    }
  }

  return {
    track: (getKeywords) => {
      tryTrack(getKeywords)
    }
  }
}
