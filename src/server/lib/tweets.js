const _ = require('lodash')
//https://developer.twitter.com/en/docs/tweets/data-dictionary/overview/tweet-object
const isTweet = _.conforms({
  created_at: _.isString,
  id_str: _.isString,
  entities: _.isObject,
  text: _.isString,
  user: _.isObject
})

const extractRelevantTexts = (tweet) => {
  let texts = []
  //toplevel
  if (tweet.extended_tweet) {
    texts.push(tweet.extended_tweet.full_text)
  } else {
    texts.push(tweet.text)
  }
  if (tweet.retweeted_status) {
    texts = texts.concat(extractRelevantTexts(tweet.retweeted_status))
  }
  if (tweet.quoted_status) {
    texts = texts.concat(extractRelevantTexts(tweet.quoted_status))
  }
  return texts
}

//Before sending to client:
const processTweet = (tweet) => {
  return {
    id: tweet.id_str,
    text: tweet.text,
    hashtags: tweet.entities.hashtags.map((hashtag) => hashtag.text),
    timestamp_ms: parseInt(tweet.timestamp_ms)
  }
}

exports.isTweet = isTweet
exports.extractRelevantTexts = extractRelevantTexts
exports.processTweet = processTweet
