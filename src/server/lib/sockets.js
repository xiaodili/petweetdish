const _ = require('lodash')

exports.getTrackedKeywords = (io) => {
  return _.keys(io.sockets.connected).map((socketId) => {
    return io.sockets.connected[socketId].keyword
  }).filter((keyword) => {
    return keyword
  })
}

exports.getNumConnections = (io) => {
  return _.keys(io.sockets.connected).length
}
