module.exports = {
  log: (...obj) => {
    console.log(new Date().toISOString(), ...obj)
  }
}
