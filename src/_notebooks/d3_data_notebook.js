var LOG_FILEPATH = (path.join(process.cwd(), 'logs/171229_js_25mins.txt'))
var events_log = fs.readFileSync(LOG_FILEPATH, 'utf8')
var events_log_munged = '[' + events_log.replace(/\}\n+\{/g, '},{') + ']'
var events = JSON.parse(events_log_munged)
events.length

var matchHashTags = (text) => {
  const matched_hts = text.match(/\S*#(?:\[[^\]]+\]|\S+)/g)
  return matched_hts
    ? matched_hts.map((ht) => {
      return ht.slice(1).toLowerCase()
    })
    : []
}

var getPairCombinations = (xs) => {
  const combinations = []
  for (let i = 0; i < xs.length - 1; i++) {
    for (let j = i + 1; j < xs.length; j++) {
      combinations.push([xs[i], xs[j]])
    }
  }
  return combinations
}

//How to make these bidirectional? Let's just see what it looks like for now...
var getGraphDataFromEvents = (tweet_stream) => {
  const tweets = {
  }
  const graph = {
    links: [],
    nodes: {
    }
  }
  for (const tweet of tweet_stream) {
    const {
      text,
      id,
      timestamp_ms
    } = tweet
    tweets[id] = {
      text,
      id,
      timestamp_ms
    }
    const hashtags = matchHashTags(text)
    for (const hashtag of hashtags) {
      if (!(hashtag in graph.nodes)) {
        graph.nodes[hashtag] = {
          id: hashtag,
          tweetIds: []
        }
      }
      graph.nodes[hashtag].tweetIds.push(id)
    }
    //links
    const links = getPairCombinations(hashtags)
    graph.links.push(...links)
  }
  return {
    tweets,
    graph: {
      links: graph.links.map((link) => {
        return {
          source: link[0],
          target: link[1]
        }
      }),
      nodes: Object.keys(graph.nodes).map((key) => {
        return graph.nodes[key]
      })
    }
  }
}

var graph_data = getGraphDataFromEvents(events)

Object.keys(graph_data)
//[ 'tweets', 'graph' ]
graph_data.tweets

graph_data.graph.links[3]
//{ source: 'programming', target: 'golang' }

graph_data.graph.nodes.length
//121

//Cool... shall we try to visualise this?

graph_data.graph.links.length
//677

var blob = 'module.exports = ' + JSON.stringify(graph_data)

fs.writeFileSync(path.join(process.cwd(), 'src/client/twitter_sample_data_for_d3.js'), blob)
