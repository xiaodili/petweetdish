const performance = require('perf_hooks').performance
const _ = require('lodash')

const maxComponentSize = (arr) => {
  if (arr.length === 0) {
    return 0
  }
  let m = arr[0].length
  for (let i = 1; i < arr.length; i++) {
    let v = arr[i].length
    if (v > m) {
      m = v
    }
  }
  return m
}

const maxComponentSize2 = (arr) => {
  return (arr.length !== 0 ? _.maxBy(arr, 'length').length : 0)
}

performance.now()
//43380940.251563

var testData = [
  [],
  [],
  [],
  [],
  [],
  [1,2,3],
  [4,3],
  [6,3,5,1,6,1,4,2,1],
  [6,3,5,1,6,1,4,2,1],
  [6,3,5,1,6,1,4,2,1,4,2,7,3,1],
  [6,3,5,1,6,1,4,2,1],
  [6,3,5,1,6,1,4,2,1],
  [6,3,5,1,6,1,4,2,1],
  [6,3,5,1,6,1,4,2,1]
]

maxComponentSize(testData)
//14
maxComponentSize2(testData)
//14

var testFn = (fn, data, times) => {
  const a = performance.now()
  for (let i = 0; i < times; i++) {
    fn(data)
  }
  return performance.now() - a
}

testFn(maxComponentSize, testData, 1000)
//0.07447099685668945

testFn(maxComponentSize2, testData, 1000)
//3.0425699949264526

testFn(maxComponentSize, testData, 10000)
//0.9676959961652756

testFn(maxComponentSize2, testData, 10000)
//11.001410998404026

testFn(maxComponentSize, testData, 100000)
//8.047695003449917

testFn(maxComponentSize2, testData, 100000)
//54.27480100095272

testFn(maxComponentSize, testData, 1000000)
//48.57261200249195

testFn(maxComponentSize2, testData, 1000000)
//460.58040899783373
