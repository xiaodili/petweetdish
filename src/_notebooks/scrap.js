const moment = require('moment')

moment.duration(process.uptime(), 'seconds').humanize()
//'2 minutes'


maxTweetsInNode([
  {tweets: [1,2,3]}
])
//3

maxTweetsInNode([
  {tweets: [1]}
])
//1

maxTweetsInNode([
])
//0
//


var catchError = () => {
  try {
    throw new Error('Status Code: ' + 420)
  } catch(e) {
    console.log("e: ", e);
    console.log("e.message: ", e.message);
  }
}

catchError()
