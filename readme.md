# Petweetdish

Accessible at [https://petweetdish.xiaodi.li](https://petweetdish.xiaodi.li).

## Features

Users can input a keyword to see real-time updates of Tweets, visualised in a 'petridish', according to their hashtags.

Visible Tweets can be filtered by selecting a node.

![Screenshot of ongoing experiment](https://bytebucket.org/xiaodili/petweetdish/raw/1793d4005c98ada72fbbadf5bbaae2f6e91e568a/assets/screenshot.png "Screenshot of ongoing experiment")

## Development

Start up client development with `npm start:client` (opens up a development server that supports hot reloading; default port is 8020).

Start up server development with `npm start:server` (opens up the server; default port is 8021) after putting in the appropriate Twitter credentials in `/config/server/development.js`.

To test remote devices, change `serverHost` in `/config/common/development.js` to your local IP address.

Run tests with `npm run test`

Run linting with `npm run lint`

## Architecture

The default access level for the [Track API](https://developer.twitter.com/en/docs/tweets/filter-realtime/overview) allows one connection; therefore the Twitter streaming client sits in a server which then passes on the Tweets to the connected clients.

### Data/Event Flow

#### Client Connection

The web client connects to server via sockets, passing in a keyword as a query parameter as it connects. On establishing the connection, the server restarts its Twitter streaming socket to take into account all the keywords its clients have listened for.

#### Incoming Tweet

When the server receives a Tweet from the Twitter streaming socket, it examines its fields for the clients' keywords, then broadcasts it to the relevant clients.

When tracking a keyword with the Track API, an incoming Tweet might be received if the keyword matches in any of the following fields:

* `text`, `extended_tweet.full_text`, `entities.hashtags.*` and `entities.urls.*`, `entities.media.*`, `users.*`
* In `quoted_status`, the above mentioned fields
* In `retweeted_status`, the above mentioned fields

For the purposes of the visualisation, a Tweet will only be passed on to the relevant keyword listeners if the keyword is present in any `text` or `full_text` fields.

This is all done in-process, without a message queue.

When the client receives the tweet, graph properties (i.e. connected components) are recalculated and the UI is updated reactively.

### Entities

#### Server

This server's purpose is to:

* Serve static files (client bundle and assets) (with [Express](https://expressjs.com/))
* Pass on incoming Tweets to the relevant clients (pulling socket with [twitter](https://www.npmjs.com/package/twitter), pushing socket with [socket.io](https://socket.io/))

There is one endpoint, `GET /api/status`, exposed for debugging purposes (returns `{numConnections, keywords, uptime, numHashtaglessTweets, numUnroutedTweets}`).

There is no authorisation, caching or underlying database.

#### Client

The web client is mostly built with [React](https://reactjs.org/), although there's an embedded [D3](https://d3js.org/) component for visualising the petridish culture.

Data flow and state management is somewhat similar to the [Redux/React](https://redux.js.org/docs/api/Store.html) architecture; there are two 'stores', a more general UI one (`/stores/ViewStore`) and one for graph data (`/stores/GraphStore`).

React components use [MobX](https://mobx.js.org/index.html) observables to react to changes to their props. The convention is that they may read off the observables exposed by the stores, but can only modify the state through the 'actions' provided in `/actions/StoreActions` (although there is no 'reducer' layer that maps action messages onto actions).

## Linting and Tests

Tests and linting should be passing; there is only coverage for `lib` functions at present.

There are no visual (component) tests.

## Assets

Created with Inkscape.

Favicon first exported as `png`, then converted with:

    convert assets/favicon.png -define icon:auto-resize=64,48,32,16 assets/favicon.ico

## Infrastructure and Deployment

The source code is currently hosted on Bitbucket, and the 'production' server is currently being hosted on an Ubuntu instance, so the current deployment process is something like:

* Run linting, tests, and build on workstation
* Copy the build onto production rig
* SSH onto production rig and restart the service

Which is done through the following scripts (in `/scripts`):

* `deploy.sh` (run on workstation)
* `setup.sh` (run on production rig if deploying for the first time)
* `release.sh` (run on production rig)

There is no CI server, build server or a staging environment in the pipeline.

### File Layout

Briefly commenting on some of the other files:

* `/.babelrc`: This is only used for the test runner (`lab` and `lab-babel`) to transpile the ES6 client source before running. Babel flags used for bundling the client are explicitly configured in the webpack configs.
* `/.eslintrc`: There's also the corresponding `.eslintrc` files in `/src/client/` and `/src/server/` that get picked up
* `/assets`: Icons and logos, and mockups (with [Inkscape](https://inkscape.org/en/)), some of which are copied into `/dist` as part of the build process
* `/issues.org`: Issue tracking (with [orgmode](https://orgmode.org/))
* `/scripts/`:
    * `dev_client.js`: Development setup for client
    * `dev_server.js`: Development setup for server
    * `tmux_dev.sh`: Sets up a development environment in [tmux](https://github.com/tmux/tmux/wiki) (runs the previous two scripts in different windows)
* `/src/`: Standard ES6/JavaScript code style with the following personal preferences: avoid omitting parenthesis in arrowed functions, and prefer the [Revealing Module pattern](https://addyosmani.com/resources/essentialjsdesignpatterns/book/#revealingmodulepatternjavascript) to classes when appropriate.
    * `/client/lib/graph.js`: Contains the graph algorithms for calculating connecting components etc.
    * `/client/ui/pages/Home/Petridish/PetridishD3.js`: Force directed graph visualisation, using the [d3-force](https://github.com/d3/d3-force) module
    * `/server/`: Some ES6 features (i.e. `import`) are not used so that Node can run `index.js` without doing another transpilation step.
* `/src/shared/`: Shared validation logic and enums between client and server
* `/tests/`
* `/webpack.*.config`: Webpack configurations for the base (`common`), `dev` and `prod` build

## Colophon

The main body font is [Open Sans](https://en.wikipedia.org/wiki/Open_Sans). The old school Twitter-esque logo is [Arista](https://www.dafont.com/arista.font). The main colour palette is:

* Primary: #0084b4
* Secondary: #eaaa67
* Text: #333333
* Background: f6f2f2
