const webpack = require('webpack')
const wconfig = require('../webpack.dev.js')
const config = require('../config/client/index.js')
const path = require('path')
const fs = require('fs')
const WebpackDevServer = require('webpack-dev-server')

const DIST_DIR = path.join(__dirname, '../dist')
const SRC_DIR = path.join(__dirname, '../src')

if (!fs.existsSync(DIST_DIR)){
  fs.mkdirSync(DIST_DIR)
}

const devServer = new WebpackDevServer(webpack(wconfig), {
  publicPath: `http://${config.serverHost}:${config.webpackDevServerPort}/`,
  historyApiFallback: true
})
devServer.listen(config.webpackDevServerPort, config.host, (err, stats) => {
  if (err) {
    console.log(err);
  }
  console.log(`listening on ${config.serverHost}:${config.webpackDevServerPort}`);
})
