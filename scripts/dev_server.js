const nodemon = require('nodemon')
const path = require('path')

nodemon({
  script: path.join(__dirname, '../src/server/index.js'),
  ignore: ['src/client/*', 'test/*']
});

nodemon.on('start', () => {
  console.log('App has started')
}).on('quit', () => {
  console.log('App has quit')
  process.exit();
}).on('restart', (files) => {
  console.log('App restarted due to: ', files)
});

