#!/bin/bash
SESSIONNAME="petweetdish"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)

tmux has-session -t $SESSIONNAME &> /dev/null

if [ $? != 0 ]; then
    pushd $ROOT_DIR
    tmux new-session -s $SESSIONNAME -n "dev" -d
    tmux send-keys -t $SESSIONNAME "vim src/server/index.js" C-m
    tmux send-keys -t $SESSIONNAME ":tabnew" C-m ":e src/client/index.js" C-m
    tmux split-window -h -t $SESSIONNAME
    tmux last-pane -t $SESSIONNAME # Go back to vim pane

    tmux new-window -n "watchers"
    tmux send-keys -t $SESSIONNAME "npm run start:client" C-m

    tmux split-window -h -t $SESSIONNAME
    tmux send-keys -t $SESSIONNAME "pushd src/server" C-m # changing cwd for nodemon
    tmux send-keys -t $SESSIONNAME "npm run start:server" C-m

    tmux next-window
fi

tmux attach -t $SESSIONNAME
