#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
pushd $ROOT_DIR;

echo "making logs directory..."
mkdir -p logs
echo "copying across upstart script..."
sudo cp scripts/petweetdish.conf /etc/init
echo "checking upstart script..."
init-checkconf /etc/init/petweetdish.conf &&
echo "looks fine. Installing npm dependencies..."
npm install --production
echo "done! Run release.sh to finish"
