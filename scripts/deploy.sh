#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
pushd $ROOT_DIR;
echo "setting environment to production..."
# BSD flavour of sed found on MacOS; remove first empty quotes if running with GNU
#sed -i '' 's/development/production/g' config/client/index.js
sed -i 's/development/production/g' config/client/index.js
echo "building a fresh dist directory..."
rm -rf dist
npm run build;
echo "setting environment back to development..."
# BSD flavour of sed found on MacOS; remove first empty quotes if running with GNU
#sed -i '' 's/production/development/g' config/client/index.js
sed -i 's/production/development/g' config/client/index.js
rm dist.zip;
echo "zipping package..."
zip -r dist.zip dist/*
echo "copying to deployment target..."
scp dist.zip eddie@petweetdish.xiaodi.li:/apps/petweetdish;
echo "done! Run release.sh on the deployment machine to finish"
popd;
