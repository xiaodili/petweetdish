#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
WEBPACK_BIN=$ROOT_DIR/node_modules/webpack/bin/webpack.js

pushd $ROOT_DIR

mkdir -p dist &&
mkdir -p dist/client &&
mkdir -p dist/assets &&
cp assets/external_link.svg dist/assets &&
cp assets/favicon.ico dist/client &&
cp assets/logo_noembed.svg dist/assets &&
$WEBPACK_BIN --config webpack.prod.js
popd
