#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ROOT_DIR=$( dirname $SCRIPT_DIR)
pushd $ROOT_DIR;

#first time run:
#mkdir -p logs
#sudo cp scripts/petweetdish.conf /etc/init
#init-checkconf /etc/init/petweetdish.conf

echo "getting up to date version of master..."
git pull origin master;
echo "setting environment to production..."
sed -i 's/development/production/g' config/server/index.js
echo "unzipping dist.zip..."
unzip dist.zip;
echo "(re)starting service..."
sudo service petweetdish restart;
echo "done!"
popd;
