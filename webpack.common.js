const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const config = {
  entry: [
    './src/client/index.js'
  ],
  output: {
    path: path.resolve(__dirname, 'dist/client'),
    filename: '[name][hash].js',
    publicPath: '/'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/client/index.html',
      title: 'Petweetdish'
    })
  ],
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [['babel-preset-env', {"modules": false}], 'react'],
          plugins: ['babel-plugin-transform-runtime', 'react-hot-loader/babel']
        }
      }
    }]
  }
};

module.exports = config
