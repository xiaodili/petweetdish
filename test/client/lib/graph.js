const Lab = require('lab')
const lab = Lab.script()
const { it } = lab
const { expect } = require('code')

const {
  updateInternalGraph,
  Connections,
  getConnectedComponents
} = require('../../../src/client/lib/graph.js')

const createEmptyInternals = () => {
  return {
    nodes: {},
    links: {},
    connections: Connections()
  }
}

const createNonEmptyInternals = () => {
  //return a-b-c
  return {
    nodes: {
      alpha: {
        id: 'alpha',
        tweets: []
      },
      beta: {
        id: 'beta',
        tweets: []
      },
      gamma: {
        id: 'gamma',
        tweets: []
      }
    },
    links: {
      'alpha-beta': {
        source: 'alpha',
        target: 'beta',
        tweets: []
      },
      'beta-gamma': {
        source: 'beta',
        target: 'gamma',
        tweets: []
      }
    },
    connections: Connections({
      alpha: ['beta'],
      beta: ['alpha', 'gamma'],
      gamma: ['beta']
    })
  }
}

lab.experiment('updateInternalGraph', () => {
  lab.test("if no hashtags, it shouldn't create any links or nodes", () => {
    const {
      nodes,
      links,
      connections
    } = createEmptyInternals()
    const tweet = {
      hashtags: []
    }
    const newD3Data = updateInternalGraph(tweet, nodes, links, connections)
    expect(Object.keys(nodes).length).to.equal(0)
    expect(Object.keys(links).length).to.equal(0)
    expect(newD3Data.nodes.length).to.equal(0)
    expect(newD3Data.links.length).to.equal(0)
    expect(newD3Data.links.length).to.equal(0)
    expect(connections.numEdges()).to.equal(0)
  })
  lab.test("if one hashtag, it should create one node", () => {
    const {
      nodes,
      links,
      connections
    } = createEmptyInternals()
    const tweet = {
      hashtags: ['ruby']
    }
    const newD3Data = updateInternalGraph(tweet, nodes, links, connections)
    expect(Object.keys(nodes).length).to.equal(1)
    expect(nodes['ruby']).to.exist()
    expect(newD3Data.nodes.length).to.equal(1)
    expect(connections.numEdges()).to.equal(0)
  })
  lab.test("if two hashtags, it should create two nodes and one link", () => {
    const {
      nodes,
      links,
      connections
    } = createEmptyInternals()
    const tweet = {
      hashtags: ['javascript', 'ruby']
    }
    const newD3Data = updateInternalGraph(tweet, nodes, links, connections)
    expect(Object.keys(nodes).length).to.equal(2)
    expect(Object.keys(links).length).to.equal(1)
    expect(nodes['javascript']).exist()
    expect(nodes['ruby']).to.exist()
    expect(newD3Data.nodes.length).to.equal(2)
    expect(newD3Data.links.length).to.equal(1)
    expect(connections.get('javascript')).to.equal(['ruby'])
    expect(connections.get('ruby')).to.equal(['javascript'])
    expect(connections.numEdges()).to.equal(1)
  })
  lab.test("incrementing updates", () => {
    const {
      nodes,
      links,
      connections
    } = createNonEmptyInternals()
    //finish the triangle
    const tweet = {
      hashtags: ['alpha', 'gamma']
    }
    const newD3Data = updateInternalGraph(tweet, nodes, links, connections)
    expect(newD3Data.nodes.length).to.equal(0)
    expect(newD3Data.links.length).to.equal(1)
    expect(connections.get('alpha')).to.equal(['beta', 'gamma'])
    expect(connections.get('gamma')).to.equal(['beta', 'alpha'])
    expect(connections.numEdges()).to.equal(3)
  })
  lab.test('hashtags should be normalised to all lowercase', () => {
    const {
      nodes,
      links,
      connections
    } = createNonEmptyInternals()
    //finish the triangle
    const tweet = {
      hashtags: ['Alpha', 'BETA']
    }
    const newD3Data = updateInternalGraph(tweet, nodes, links, connections)
    expect(newD3Data.nodes.length).to.equal(0)
    expect(newD3Data.links.length).to.equal(0)
  })
})
lab.experiment('getConnectedComponents', () => {
  lab.test('no connections', () => {
    const nodeIds = ['a', 'b', 'c']
    const connections = Connections()
    const expectedNumComponents = 3
    const connectedComponents = getConnectedComponents(nodeIds, connections)
    expect(connectedComponents.length).to.equal(expectedNumComponents)
  })
  lab.test('two connected components', () => {
    const nodeIds = ['a', 'b', 'c']
    const connections = Connections({
      a: ['b'],
      b: ['a']
    })
    const expectedNumComponents = 2
    const connectedComponents = getConnectedComponents(nodeIds, connections)
    expect(connectedComponents.length).to.equal(expectedNumComponents)
  })
  lab.test('fully connected', () => {
    const nodeIds = ['a', 'b', 'c']
    const connections = Connections({
      a: ['b', 'c'],
      b: ['a', 'c'],
      c: ['a', 'b']
    })
    const expectedNumComponents = 1
    const connectedComponents = getConnectedComponents(nodeIds, connections)
    expect(connectedComponents.length).to.equal(expectedNumComponents)
  })
})

exports.lab = lab
