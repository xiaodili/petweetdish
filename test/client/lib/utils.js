const Lab = require('lab')
const lab = Lab.script()
const { it } = lab
const { expect } = require('code')

const {
  dimensions
} = require('../../../src/client/ui/styling/constants.js')

const {
  createLink,
  createNode,
  getPairCombinations,
  generateLinkData,
  maxComponentSize,
  maxTweetsInNode
} = require('../../../src/client/lib/utils.js')

lab.experiment('faster max functions', () => {
  lab.test('max components work correctly', () => {
    expect(maxComponentSize([])).to.equal(0)
    expect(maxComponentSize([[1]])).to.equal(1)
      expect(maxComponentSize([
        [1],
        [1, 2, 3, 5],
        [1, 2, 3, 5, 1, 2, 5]
      ])).to.equal(7)
  })
  lab.test('max tweets work correctly', () => {
    expect(maxTweetsInNode([])).to.equal(0)
    expect(maxTweetsInNode([{tweets: [1]}])).to.equal(1)
      expect(maxTweetsInNode([
        {tweets: [1]},
        {tweets: [1, 2, 3, 5]},
        {tweets: [1, 2, 3, 5, 1, 2, 5]}
      ])).to.equal(7)
  })
})

lab.experiment('Node and link generation', () => {
  it('should generate and position nodes correctly', () => {
    const nodeId = 'javascript'
    const node = createNode(nodeId)
    const centreX = dimensions.vizContainerWidth/2
    const centreY = dimensions.vizContainerHeight/2
    expect(node.id).to.equal(nodeId)
    expect(node.tweets.peek()).to.equal([])
    expect(node.x).to.equal(centreX)
    expect(node.y).to.equal(centreY)
  })
  it('should generate link data correctly', () => {
    const linkA = 'apples'
    const linkB = 'bananas'
    const expectedId = `${linkA}-${linkB}`
    const linkData1 = generateLinkData(linkA, linkB)
    const linkData2 = generateLinkData(linkB, linkA)
    expect(linkData1.linkId).to.equal(expectedId)
    expect(linkData2.linkId).to.equal(expectedId)
    expect(linkData1.source).to.equal(linkData2.source)
    expect(linkData1.target).to.equal(linkData2.target)
  })
  it('should create links correctly', () => {
    const linkA = 'apples'
    const linkB = 'bananas'
    const link = createLink('doesntmatter', linkA, linkB)
    expect(link.source).to.equal(linkA)
    expect(link.target).to.equal(linkB)
    expect(link.tweets.peek()).to.equal([])
  })
})

lab.experiment('getPairCombinations', () => {
  it('should produce pair combinations correctly', () => {
    //base
    expect(getPairCombinations([])).to.equal([])
    //trivial
    expect(getPairCombinations([1,2])).to.equal([[1,2]])
    //nontrivial
    expect(getPairCombinations([1,2,3])).to.equal([[1,2], [1,3], [2,3]])
  })
})

exports.lab = lab
