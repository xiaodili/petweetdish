const Lab = require('lab')
const lab = Lab.script()
const { it } = lab
const { expect } = require('code')

const {
  isTweet,
  extractRelevantTexts
} = require('../../../src/server/lib/tweets.js')

const tweet1 = {
  "created_at": "Tue Jan 09 19:00:44 +0000 2018",
  "id": 950804550646104000,
  "id_str": "950804550646104064",
  "text": "#Cats #CatLover 😊😘😸 https://t.co/lkrWFrr0dn",
  "display_text_range": [
    0,
    19
  ],
  "source": "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
  "truncated": false,
  "in_reply_to_status_id": null,
  "in_reply_to_status_id_str": null,
  "in_reply_to_user_id": null,
  "in_reply_to_user_id_str": null,
  "in_reply_to_screen_name": null,
  "user": {
    "id": 797098626992472000,
    "id_str": "797098626992472065",
    "name": "AnneRoth",
    "screen_name": "AnneRoth1983",
    "location": "Österreich",
    "url": null,
    "description": "Tattooed Bookseller❤Bibliophile📚💙❤️Fantasy&Thriller💜Tom Hiddleston💙VW Bug❤️Cats💜Chameleon❤️Coffeelover☕ #onlygoodVibes,please😉",
    "translator_type": "none",
    "protected": false,
    "verified": false,
    "followers_count": 750,
    "friends_count": 565,
    "listed_count": 18,
    "favourites_count": 76464,
    "statuses_count": 23954,
    "created_at": "Fri Nov 11 15:28:16 +0000 2016",
    "utc_offset": null,
    "time_zone": null,
    "geo_enabled": false,
    "lang": "de",
    "contributors_enabled": false,
    "is_translator": false,
    "profile_background_color": "F5F8FA",
    "profile_background_image_url": "",
    "profile_background_image_url_https": "",
    "profile_background_tile": false,
    "profile_link_color": "1DA1F2",
    "profile_sidebar_border_color": "C0DEED",
    "profile_sidebar_fill_color": "DDEEF6",
    "profile_text_color": "333333",
    "profile_use_background_image": true,
    "profile_image_url": "http://pbs.twimg.com/profile_images/884068154854567936/UmUt1JVY_normal.jpg",
    "profile_image_url_https": "https://pbs.twimg.com/profile_images/884068154854567936/UmUt1JVY_normal.jpg",
    "profile_banner_url": "https://pbs.twimg.com/profile_banners/797098626992472065/1480620784",
    "default_profile": true,
    "default_profile_image": false,
    "following": null,
    "follow_request_sent": null,
    "notifications": null
  },
  "geo": null,
  "coordinates": null,
  "place": null,
  "contributors": null,
  "quoted_status_id": 950797211176226800,
  "quoted_status_id_str": "950797211176226817",
  "quoted_status": {
    "created_at": "Tue Jan 09 18:31:34 +0000 2018",
    "id": 950797211176226800,
    "id_str": "950797211176226817",
    "text": "https://t.co/4NxoP5AkgM",
    "display_text_range": [
      0,
      0
    ],
    "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
    "truncated": false,
    "in_reply_to_status_id": null,
    "in_reply_to_status_id_str": null,
    "in_reply_to_user_id": null,
    "in_reply_to_user_id_str": null,
    "in_reply_to_screen_name": null,
    "user": {
      "id": 3297843805,
      "id_str": "3297843805",
      "name": "Thomas William Hiddleston",
      "screen_name": "hiddleslove63",
      "location": "Where my beloved is.",
      "url": null,
      "description": "❄You brought me sunshine,\nwhen I only saw rain.To him I belong fully,my moon,my husband,my guider @LokiSilberzunge {28/08/2016}❄ RP{+18}| #Gay|#Tomki|#SV|#Taken",
      "translator_type": "regular",
      "protected": false,
      "verified": false,
      "followers_count": 1283,
      "friends_count": 1255,
      "listed_count": 19,
      "favourites_count": 9246,
      "statuses_count": 54930,
      "created_at": "Mon Jul 27 14:07:47 +0000 2015",
      "utc_offset": -28800,
      "time_zone": "Pacific Time (US & Canada)",
      "geo_enabled": true,
      "lang": "ro",
      "contributors_enabled": false,
      "is_translator": false,
      "profile_background_color": "C0DEED",
      "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/630786829994860549/jQ8BG_rB.jpg",
      "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/630786829994860549/jQ8BG_rB.jpg",
      "profile_background_tile": false,
      "profile_link_color": "0084B4",
      "profile_sidebar_border_color": "000000",
      "profile_sidebar_fill_color": "000000",
      "profile_text_color": "000000",
      "profile_use_background_image": true,
      "profile_image_url": "http://pbs.twimg.com/profile_images/949383431145783296/TjA4Wvp2_normal.jpg",
      "profile_image_url_https": "https://pbs.twimg.com/profile_images/949383431145783296/TjA4Wvp2_normal.jpg",
      "profile_banner_url": "https://pbs.twimg.com/profile_banners/3297843805/1481918481",
      "default_profile": false,
      "default_profile_image": false,
      "following": null,
      "follow_request_sent": null,
      "notifications": null
    },
    "geo": null,
    "coordinates": null,
    "place": null,
    "contributors": null,
    "is_quote_status": false,
    "quote_count": 0,
    "reply_count": 0,
    "retweet_count": 0,
    "favorite_count": 1,
    "entities": {
      "hashtags": [],
      "urls": [],
      "user_mentions": [],
      "symbols": [],
      "media": [
        {
          "id": 950797199037861900,
          "id_str": "950797199037861888",
          "indices": [
            0,
            23
          ],
          "media_url": "http://pbs.twimg.com/tweet_video_thumb/DTHpA9YWkAAutVv.jpg",
          "media_url_https": "https://pbs.twimg.com/tweet_video_thumb/DTHpA9YWkAAutVv.jpg",
          "url": "https://t.co/4NxoP5AkgM",
          "display_url": "pic.twitter.com/4NxoP5AkgM",
          "expanded_url": "https://twitter.com/hiddleslove63/status/950797211176226817/photo/1",
          "type": "photo",
          "sizes": {
            "thumb": {
              "w": 150,
              "h": 150,
              "resize": "crop"
            },
            "large": {
              "w": 262,
              "h": 262,
              "resize": "fit"
            },
            "small": {
              "w": 262,
              "h": 262,
              "resize": "fit"
            },
            "medium": {
              "w": 262,
              "h": 262,
              "resize": "fit"
            }
          }
        }
      ]
    },
    "extended_entities": {
      "media": [
        {
          "id": 950797199037861900,
          "id_str": "950797199037861888",
          "indices": [
            0,
            23
          ],
          "media_url": "http://pbs.twimg.com/tweet_video_thumb/DTHpA9YWkAAutVv.jpg",
          "media_url_https": "https://pbs.twimg.com/tweet_video_thumb/DTHpA9YWkAAutVv.jpg",
          "url": "https://t.co/4NxoP5AkgM",
          "display_url": "pic.twitter.com/4NxoP5AkgM",
          "expanded_url": "https://twitter.com/hiddleslove63/status/950797211176226817/photo/1",
          "type": "animated_gif",
          "sizes": {
            "thumb": {
              "w": 150,
              "h": 150,
              "resize": "crop"
            },
            "large": {
              "w": 262,
              "h": 262,
              "resize": "fit"
            },
            "small": {
              "w": 262,
              "h": 262,
              "resize": "fit"
            },
            "medium": {
              "w": 262,
              "h": 262,
              "resize": "fit"
            }
          },
          "video_info": {
            "aspect_ratio": [
              1,
              1
            ],
            "variants": [
              {
                "bitrate": 0,
                "content_type": "video/mp4",
                "url": "https://video.twimg.com/tweet_video/DTHpA9YWkAAutVv.mp4"
              }
            ]
          }
        }
      ]
    },
    "favorited": false,
    "retweeted": false,
    "possibly_sensitive": false,
    "filter_level": "low",
    "lang": "und"
  },
  "is_quote_status": true,
  "quote_count": 0,
  "reply_count": 0,
  "retweet_count": 0,
  "favorite_count": 0,
  "entities": {
    "hashtags": [
      {
        "text": "Cats",
        "indices": [
          0,
          5
        ]
      },
      {
        "text": "CatLover",
        "indices": [
          6,
          15
        ]
      }
    ],
    "urls": [
      {
        "url": "https://t.co/lkrWFrr0dn",
        "expanded_url": "https://twitter.com/hiddleslove63/status/950797211176226817",
        "display_url": "twitter.com/hiddleslove63/…",
        "indices": [
          20,
          43
        ]
      }
    ],
    "user_mentions": [],
    "symbols": []
  },
  "favorited": false,
  "retweeted": false,
  "possibly_sensitive": false,
  "filter_level": "low",
  "lang": "und",
  "timestamp_ms": "1515524444625"
}

const tweet2 = {
  "created_at": "Tue Jan 09 19:45:26 +0000 2018",
  "id": 950815798905442300,
  "id_str": "950815798905442305",
  "text": "RT @SomConnexio: Desmuntant rumors sobre la Banca Ètica\n#TriaBancaÈtica https://t.co/vNJuz79V4x",
  "source": "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
  "truncated": false,
  "in_reply_to_status_id": null,
  "in_reply_to_status_id_str": null,
  "in_reply_to_user_id": null,
  "in_reply_to_user_id_str": null,
  "in_reply_to_screen_name": null,
  "user": {
    "id": 2499156162,
    "id_str": "2499156162",
    "name": "Peter Solar",
    "screen_name": "PereSoriaAlcaza",
    "location": "Terrassa",
    "url": null,
    "description": "Working in Renewable Energy since 1993. Expressing here only personal opinions",
    "translator_type": "none",
    "protected": false,
    "verified": false,
    "followers_count": 1553,
    "friends_count": 2206,
    "listed_count": 259,
    "favourites_count": 2127,
    "statuses_count": 22038,
    "created_at": "Fri May 16 15:34:45 +0000 2014",
    "utc_offset": null,
    "time_zone": null,
    "geo_enabled": false,
    "lang": "es",
    "contributors_enabled": false,
    "is_translator": false,
    "profile_background_color": "C0DEED",
    "profile_background_image_url": "http://abs.twimg.com/images/themes/theme1/bg.png",
    "profile_background_image_url_https": "https://abs.twimg.com/images/themes/theme1/bg.png",
    "profile_background_tile": false,
    "profile_link_color": "B30000",
    "profile_sidebar_border_color": "C0DEED",
    "profile_sidebar_fill_color": "DDEEF6",
    "profile_text_color": "333333",
    "profile_use_background_image": true,
    "profile_image_url": "http://pbs.twimg.com/profile_images/924598477685968896/CbXF3-I2_normal.jpg",
    "profile_image_url_https": "https://pbs.twimg.com/profile_images/924598477685968896/CbXF3-I2_normal.jpg",
    "profile_banner_url": "https://pbs.twimg.com/profile_banners/2499156162/1509276577",
    "default_profile": false,
    "default_profile_image": false,
    "following": null,
    "follow_request_sent": null,
    "notifications": null
  },
  "geo": null,
  "coordinates": null,
  "place": null,
  "contributors": null,
  "retweeted_status": {
    "created_at": "Tue Jan 09 18:32:00 +0000 2018",
    "id": 950797320592949200,
    "id_str": "950797320592949249",
    "text": "Desmuntant rumors sobre la Banca Ètica\n#TriaBancaÈtica https://t.co/vNJuz79V4x",
    "display_text_range": [
      0,
      54
    ],
    "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
    "truncated": false,
    "in_reply_to_status_id": null,
    "in_reply_to_status_id_str": null,
    "in_reply_to_user_id": null,
    "in_reply_to_user_id_str": null,
    "in_reply_to_screen_name": null,
    "user": {
      "id": 1364372725,
      "id_str": "1364372725",
      "name": "Som Connexió",
      "screen_name": "SomConnexio",
      "location": null,
      "url": "http://www.somconnexio.coop",
      "description": "Una cooperativa de consumidores de telefonia i connexió internet que aplega persones per avançar cap a la sobirania de les telecomunicacions",
      "translator_type": "none",
      "protected": false,
      "verified": false,
      "followers_count": 7826,
      "friends_count": 1760,
      "listed_count": 217,
      "favourites_count": 1931,
      "statuses_count": 2858,
      "created_at": "Fri Apr 19 13:01:20 +0000 2013",
      "utc_offset": 7200,
      "time_zone": "Athens",
      "geo_enabled": false,
      "lang": "ca",
      "contributors_enabled": false,
      "is_translator": false,
      "profile_background_color": "850F66",
      "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/599512457363607552/dw0HAu55.jpg",
      "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/599512457363607552/dw0HAu55.jpg",
      "profile_background_tile": false,
      "profile_link_color": "934363",
      "profile_sidebar_border_color": "FFFFFF",
      "profile_sidebar_fill_color": "7AC3EE",
      "profile_text_color": "3D1957",
      "profile_use_background_image": true,
      "profile_image_url": "http://pbs.twimg.com/profile_images/892687070031990784/qUMgehn8_normal.jpg",
      "profile_image_url_https": "https://pbs.twimg.com/profile_images/892687070031990784/qUMgehn8_normal.jpg",
      "profile_banner_url": "https://pbs.twimg.com/profile_banners/1364372725/1501668126",
      "default_profile": false,
      "default_profile_image": false,
      "following": null,
      "follow_request_sent": null,
      "notifications": null
    },
    "geo": null,
    "coordinates": null,
    "place": null,
    "contributors": null,
    "quoted_status_id": 950693267909247000,
    "quoted_status_id_str": "950693267909246976",
    "quoted_status": {
      "created_at": "Tue Jan 09 11:38:32 +0000 2018",
      "id": 950693267909247000,
      "id_str": "950693267909246976",
      "text": "Desmuntant tòpics sobre la banca ètica. \n❓ La banca ètica ofereix els mateixos serveis que els bancs tradicionals?… https://t.co/f6OdxNtYg5",
      "display_text_range": [
        0,
        140
      ],
      "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
      "truncated": true,
      "in_reply_to_status_id": null,
      "in_reply_to_status_id_str": null,
      "in_reply_to_user_id": null,
      "in_reply_to_user_id_str": null,
      "in_reply_to_screen_name": null,
      "user": {
        "id": 278563282,
        "id_str": "278563282",
        "name": "FETS.org",
        "screen_name": "banca_etica",
        "location": "Catalunya",
        "url": "http://www.fets.org",
        "description": "Finançament Ètic i Solidari - Som una associació que promovem el finançament ètic i solidari, com a efecte transformador de la nostra societat",
        "translator_type": "none",
        "protected": false,
        "verified": false,
        "followers_count": 6724,
        "friends_count": 1998,
        "listed_count": 257,
        "favourites_count": 1022,
        "statuses_count": 2818,
        "created_at": "Thu Apr 07 14:34:47 +0000 2011",
        "utc_offset": 3600,
        "time_zone": "Madrid",
        "geo_enabled": true,
        "lang": "ca",
        "contributors_enabled": false,
        "is_translator": false,
        "profile_background_color": "EC873A",
        "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/378800000032838112/eafd399b7261ee4bc55c6b163e19f9f6.jpeg",
        "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/378800000032838112/eafd399b7261ee4bc55c6b163e19f9f6.jpeg",
        "profile_background_tile": false,
        "profile_link_color": "0084B4",
        "profile_sidebar_border_color": "FFFFFF",
        "profile_sidebar_fill_color": "DDEEF6",
        "profile_text_color": "333333",
        "profile_use_background_image": true,
        "profile_image_url": "http://pbs.twimg.com/profile_images/2201129308/logo_normal.jpg",
        "profile_image_url_https": "https://pbs.twimg.com/profile_images/2201129308/logo_normal.jpg",
        "default_profile": false,
        "default_profile_image": false,
        "following": null,
        "follow_request_sent": null,
        "notifications": null
      },
      "geo": null,
      "coordinates": null,
      "place": null,
      "contributors": null,
      "is_quote_status": false,
      "extended_tweet": {
        "full_text": "Desmuntant tòpics sobre la banca ètica. \n❓ La banca ètica ofereix els mateixos serveis que els bancs tradicionals? Tenen caixers? Són segurs? I rendibles?💰 https://t.co/eMLzp0CeK2 Per @banca_etica i @SentitCritic https://t.co/8LZTeFuA2f",
        "display_text_range": [
          0,
          212
        ],
        "entities": {
          "hashtags": [],
          "urls": [
            {
              "url": "https://t.co/eMLzp0CeK2",
              "expanded_url": "http://www.bancaetica.cat",
              "display_url": "bancaetica.cat",
              "indices": [
                156,
                179
              ]
            }
          ],
          "user_mentions": [
            {
              "screen_name": "banca_etica",
              "name": "FETS.org",
              "id": 278563282,
              "id_str": "278563282",
              "indices": [
                184,
                196
              ]
            },
            {
              "screen_name": "SentitCritic",
              "name": "elcritic.cat",
              "id": 1548418610,
              "id_str": "1548418610",
              "indices": [
                199,
                212
              ]
            }
          ],
          "symbols": [],
          "media": [
            {
              "id": 950692701191761900,
              "id_str": "950692701191761920",
              "indices": [
                213,
                236
              ],
              "media_url": "http://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
              "media_url_https": "https://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
              "url": "https://t.co/8LZTeFuA2f",
              "display_url": "pic.twitter.com/8LZTeFuA2f",
              "expanded_url": "https://twitter.com/banca_etica/status/950693267909246976/video/1",
              "type": "video",
              "sizes": {
                "thumb": {
                  "w": 150,
                  "h": 150,
                  "resize": "crop"
                },
                "medium": {
                  "w": 1200,
                  "h": 675,
                  "resize": "fit"
                },
                "small": {
                  "w": 680,
                  "h": 383,
                  "resize": "fit"
                },
                "large": {
                  "w": 1280,
                  "h": 720,
                  "resize": "fit"
                }
              },
              "video_info": {
                "aspect_ratio": [
                  16,
                  9
                ],
                "duration_millis": 114033,
                "variants": [
                  {
                    "content_type": "application/x-mpegURL",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/pl/i3s07tiRKsxwKmkP.m3u8"
                  },
                  {
                    "bitrate": 2176000,
                    "content_type": "video/mp4",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/1280x720/aLM-xLV4W3BAKesk.mp4"
                  },
                  {
                    "bitrate": 320000,
                    "content_type": "video/mp4",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/320x180/P9lGJ6ODz2DMhzW4.mp4"
                  },
                  {
                    "bitrate": 832000,
                    "content_type": "video/mp4",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/640x360/HNjaagK2R1pujh8_.mp4"
                  }
                ]
              }
            }
          ]
        },
        "extended_entities": {
          "media": [
            {
              "id": 950692701191761900,
              "id_str": "950692701191761920",
              "indices": [
                213,
                236
              ],
              "media_url": "http://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
              "media_url_https": "https://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
              "url": "https://t.co/8LZTeFuA2f",
              "display_url": "pic.twitter.com/8LZTeFuA2f",
              "expanded_url": "https://twitter.com/banca_etica/status/950693267909246976/video/1",
              "type": "video",
              "sizes": {
                "thumb": {
                  "w": 150,
                  "h": 150,
                  "resize": "crop"
                },
                "medium": {
                  "w": 1200,
                  "h": 675,
                  "resize": "fit"
                },
                "small": {
                  "w": 680,
                  "h": 383,
                  "resize": "fit"
                },
                "large": {
                  "w": 1280,
                  "h": 720,
                  "resize": "fit"
                }
              },
              "video_info": {
                "aspect_ratio": [
                  16,
                  9
                ],
                "duration_millis": 114033,
                "variants": [
                  {
                    "content_type": "application/x-mpegURL",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/pl/i3s07tiRKsxwKmkP.m3u8"
                  },
                  {
                    "bitrate": 2176000,
                    "content_type": "video/mp4",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/1280x720/aLM-xLV4W3BAKesk.mp4"
                  },
                  {
                    "bitrate": 320000,
                    "content_type": "video/mp4",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/320x180/P9lGJ6ODz2DMhzW4.mp4"
                  },
                  {
                    "bitrate": 832000,
                    "content_type": "video/mp4",
                    "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/640x360/HNjaagK2R1pujh8_.mp4"
                  }
                ]
              }
            }
          ]
        }
      },
      "quote_count": 6,
      "reply_count": 0,
      "retweet_count": 39,
      "favorite_count": 29,
      "entities": {
        "hashtags": [],
        "urls": [
          {
            "url": "https://t.co/f6OdxNtYg5",
            "expanded_url": "https://twitter.com/i/web/status/950693267909246976",
            "display_url": "twitter.com/i/web/status/9…",
            "indices": [
              116,
              139
            ]
          }
        ],
        "user_mentions": [],
        "symbols": []
      },
      "favorited": false,
      "retweeted": false,
      "possibly_sensitive": false,
      "filter_level": "low",
      "lang": "und"
    },
    "is_quote_status": true,
    "quote_count": 0,
    "reply_count": 0,
    "retweet_count": 9,
    "favorite_count": 6,
    "entities": {
      "hashtags": [
        {
          "text": "TriaBancaÈtica",
          "indices": [
            39,
            54
          ]
        }
      ],
      "urls": [
        {
          "url": "https://t.co/vNJuz79V4x",
          "expanded_url": "https://twitter.com/banca_etica/status/950693267909246976",
          "display_url": "twitter.com/banca_etica/st…",
          "indices": [
            55,
            78
          ]
        }
      ],
      "user_mentions": [],
      "symbols": []
    },
    "favorited": false,
    "retweeted": false,
    "possibly_sensitive": false,
    "filter_level": "low",
    "lang": "und"
  },
  "quoted_status_id": 950693267909247000,
  "quoted_status_id_str": "950693267909246976",
  "quoted_status": {
    "created_at": "Tue Jan 09 11:38:32 +0000 2018",
    "id": 950693267909247000,
    "id_str": "950693267909246976",
    "text": "Desmuntant tòpics sobre la banca ètica. \n❓ La banca ètica ofereix els mateixos serveis que els bancs tradicionals?… https://t.co/f6OdxNtYg5",
    "display_text_range": [
      0,
      140
    ],
    "source": "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
    "truncated": true,
    "in_reply_to_status_id": null,
    "in_reply_to_status_id_str": null,
    "in_reply_to_user_id": null,
    "in_reply_to_user_id_str": null,
    "in_reply_to_screen_name": null,
    "user": {
      "id": 278563282,
      "id_str": "278563282",
      "name": "FETS.org",
      "screen_name": "banca_etica",
      "location": "Catalunya",
      "url": "http://www.fets.org",
      "description": "Finançament Ètic i Solidari - Som una associació que promovem el finançament ètic i solidari, com a efecte transformador de la nostra societat",
      "translator_type": "none",
      "protected": false,
      "verified": false,
      "followers_count": 6724,
      "friends_count": 1998,
      "listed_count": 257,
      "favourites_count": 1022,
      "statuses_count": 2818,
      "created_at": "Thu Apr 07 14:34:47 +0000 2011",
      "utc_offset": 3600,
      "time_zone": "Madrid",
      "geo_enabled": true,
      "lang": "ca",
      "contributors_enabled": false,
      "is_translator": false,
      "profile_background_color": "EC873A",
      "profile_background_image_url": "http://pbs.twimg.com/profile_background_images/378800000032838112/eafd399b7261ee4bc55c6b163e19f9f6.jpeg",
      "profile_background_image_url_https": "https://pbs.twimg.com/profile_background_images/378800000032838112/eafd399b7261ee4bc55c6b163e19f9f6.jpeg",
      "profile_background_tile": false,
      "profile_link_color": "0084B4",
      "profile_sidebar_border_color": "FFFFFF",
      "profile_sidebar_fill_color": "DDEEF6",
      "profile_text_color": "333333",
      "profile_use_background_image": true,
      "profile_image_url": "http://pbs.twimg.com/profile_images/2201129308/logo_normal.jpg",
      "profile_image_url_https": "https://pbs.twimg.com/profile_images/2201129308/logo_normal.jpg",
      "default_profile": false,
      "default_profile_image": false,
      "following": null,
      "follow_request_sent": null,
      "notifications": null
    },
    "geo": null,
    "coordinates": null,
    "place": null,
    "contributors": null,
    "is_quote_status": false,
    "extended_tweet": {
      "full_text": "Desmuntant tòpics sobre la banca ètica. \n❓ La banca ètica ofereix els mateixos serveis que els bancs tradicionals? Tenen caixers? Són segurs? I rendibles?💰 https://t.co/eMLzp0CeK2 Per @banca_etica i @SentitCritic https://t.co/8LZTeFuA2f",
      "display_text_range": [
        0,
        212
      ],
      "entities": {
        "hashtags": [],
        "urls": [
          {
            "url": "https://t.co/eMLzp0CeK2",
            "expanded_url": "http://www.bancaetica.cat",
            "display_url": "bancaetica.cat",
            "indices": [
              156,
              179
            ]
          }
        ],
        "user_mentions": [
          {
            "screen_name": "banca_etica",
            "name": "FETS.org",
            "id": 278563282,
            "id_str": "278563282",
            "indices": [
              184,
              196
            ]
          },
          {
            "screen_name": "SentitCritic",
            "name": "elcritic.cat",
            "id": 1548418610,
            "id_str": "1548418610",
            "indices": [
              199,
              212
            ]
          }
        ],
        "symbols": [],
        "media": [
          {
            "id": 950692701191761900,
            "id_str": "950692701191761920",
            "indices": [
              213,
              236
            ],
            "media_url": "http://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
            "media_url_https": "https://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
            "url": "https://t.co/8LZTeFuA2f",
            "display_url": "pic.twitter.com/8LZTeFuA2f",
            "expanded_url": "https://twitter.com/banca_etica/status/950693267909246976/video/1",
            "type": "video",
            "sizes": {
              "thumb": {
                "w": 150,
                "h": 150,
                "resize": "crop"
              },
              "medium": {
                "w": 1200,
                "h": 675,
                "resize": "fit"
              },
              "small": {
                "w": 680,
                "h": 383,
                "resize": "fit"
              },
              "large": {
                "w": 1280,
                "h": 720,
                "resize": "fit"
              }
            },
            "video_info": {
              "aspect_ratio": [
                16,
                9
              ],
              "duration_millis": 114033,
              "variants": [
                {
                  "content_type": "application/x-mpegURL",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/pl/i3s07tiRKsxwKmkP.m3u8"
                },
                {
                  "bitrate": 2176000,
                  "content_type": "video/mp4",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/1280x720/aLM-xLV4W3BAKesk.mp4"
                },
                {
                  "bitrate": 320000,
                  "content_type": "video/mp4",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/320x180/P9lGJ6ODz2DMhzW4.mp4"
                },
                {
                  "bitrate": 832000,
                  "content_type": "video/mp4",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/640x360/HNjaagK2R1pujh8_.mp4"
                }
              ]
            }
          }
        ]
      },
      "extended_entities": {
        "media": [
          {
            "id": 950692701191761900,
            "id_str": "950692701191761920",
            "indices": [
              213,
              236
            ],
            "media_url": "http://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
            "media_url_https": "https://pbs.twimg.com/ext_tw_video_thumb/950692701191761920/pu/img/6PmP-09IiL2NPhTX.jpg",
            "url": "https://t.co/8LZTeFuA2f",
            "display_url": "pic.twitter.com/8LZTeFuA2f",
            "expanded_url": "https://twitter.com/banca_etica/status/950693267909246976/video/1",
            "type": "video",
            "sizes": {
              "thumb": {
                "w": 150,
                "h": 150,
                "resize": "crop"
              },
              "medium": {
                "w": 1200,
                "h": 675,
                "resize": "fit"
              },
              "small": {
                "w": 680,
                "h": 383,
                "resize": "fit"
              },
              "large": {
                "w": 1280,
                "h": 720,
                "resize": "fit"
              }
            },
            "video_info": {
              "aspect_ratio": [
                16,
                9
              ],
              "duration_millis": 114033,
              "variants": [
                {
                  "content_type": "application/x-mpegURL",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/pl/i3s07tiRKsxwKmkP.m3u8"
                },
                {
                  "bitrate": 2176000,
                  "content_type": "video/mp4",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/1280x720/aLM-xLV4W3BAKesk.mp4"
                },
                {
                  "bitrate": 320000,
                  "content_type": "video/mp4",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/320x180/P9lGJ6ODz2DMhzW4.mp4"
                },
                {
                  "bitrate": 832000,
                  "content_type": "video/mp4",
                  "url": "https://video.twimg.com/ext_tw_video/950692701191761920/pu/vid/640x360/HNjaagK2R1pujh8_.mp4"
                }
              ]
            }
          }
        ]
      }
    },
    "quote_count": 6,
    "reply_count": 0,
    "retweet_count": 39,
    "favorite_count": 29,
    "entities": {
      "hashtags": [],
      "urls": [
        {
          "url": "https://t.co/f6OdxNtYg5",
          "expanded_url": "https://twitter.com/i/web/status/950693267909246976",
          "display_url": "twitter.com/i/web/status/9…",
          "indices": [
            116,
            139
          ]
        }
      ],
      "user_mentions": [],
      "symbols": []
    },
    "favorited": false,
    "retweeted": false,
    "possibly_sensitive": false,
    "filter_level": "low",
    "lang": "und"
  },
  "is_quote_status": true,
  "quote_count": 0,
  "reply_count": 0,
  "retweet_count": 0,
  "favorite_count": 0,
  "entities": {
    "hashtags": [
      {
        "text": "TriaBancaÈtica",
        "indices": [
          56,
          71
        ]
      }
    ],
    "urls": [
      {
        "url": "https://t.co/vNJuz79V4x",
        "expanded_url": "https://twitter.com/banca_etica/status/950693267909246976",
        "display_url": "twitter.com/banca_etica/st…",
        "indices": [
          72,
          95
        ]
      }
    ],
    "user_mentions": [
      {
        "screen_name": "SomConnexio",
        "name": "Som Connexió",
        "id": 1364372725,
        "id_str": "1364372725",
        "indices": [
          3,
          15
        ]
      }
    ],
    "symbols": []
  },
  "favorited": false,
  "retweeted": false,
  "possibly_sensitive": false,
  "filter_level": "low",
  "lang": "und",
  "timestamp_ms": "1515527126419"
}


lab.experiment('Tweets', () => {
  lab.test('identifying Tweet correctly', () => {
    expect(isTweet(tweet1)).to.be.true()
    expect(isTweet(tweet2)).to.be.true()
  })
  lab.test('extracting relevant text fields from tweets', () => {
    expect(extractRelevantTexts(tweet1)).to.equal([
      tweet1.text,
      tweet1.quoted_status.text
    ])
    expect(extractRelevantTexts(tweet2)).to.equal([
      tweet2.text,
      tweet2.retweeted_status.text,
      tweet2.retweeted_status.quoted_status.extended_tweet.full_text,
      tweet2.quoted_status.extended_tweet.full_text
    ])
  })
})

exports.lab = lab
