const common = require('../common/development.js')
module.exports = {
  webpackDevServerPort: 8020, //for development
  serverHost: common.serverHost, //webpack dev server needs this
  serverUrl: `http://${common.serverHost}:${common.serverPort}`
}
