const common = require('../common/development.js')
module.exports = {
  serverPort: common.serverPort,
  twitterConfig: {
    consumer_key: 'consumer_key',
    consumer_secret: 'consumer_secret',
    access_token_key: 'access_token_key',
    access_token_secret: 'access_token_secret'
  }
}
