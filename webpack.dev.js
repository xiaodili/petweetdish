const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const webpack = require('webpack')
const config = require('./config/client/index.js')

module.exports = merge.strategy({
  entry: 'prepend' })(common, {
    entry: [
      'react-hot-loader/patch',
      `webpack-dev-server/client?http://${config.serverHost}:${config.webpackDevServerPort}`,
      'webpack/hot/only-dev-server'
    ],
    watchOptions: {
      ignored: ['node_modules']
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin()
      //new webpack.optimize.CommonsChunkPlugin({
      //name: 'common'
      //}),
      //new webpack.optimize.CommonsChunkPlugin({
      //name: 'vendor',
      //minChunks: function (module) {
      //return module.context && module.context.indexOf('node_modules') !== -1;
      //}
      //}),
      //new webpack.optimize.CommonsChunkPlugin({
      //name: 'manifest'
      //})
    ],
    //https://webpack.js.org/configuration/devtool/
    devtool: 'cheap-module-eval-source-map'
  })

